#!/bin/bash

python3.5 ~/projects/ddd/src/image_detector_ex.py \
--model ~/projects/ddd/models/mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite \
--label ~/projects/ddd/models/coco_labels.txt \
--input ~/projects/ddd/data/images/vlcsnap-2019-04-17-16h22m12s191.png \
