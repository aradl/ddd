'''
    Wrapper for object tracking

    inspired from:
        https://www.pyimagesearch.com/2018/10/29/multi-object-tracking-with-dlib/
        https://www.pyimagesearch.com/2018/08/13/opencv-people-counter/
'''
import cv2 
import time
import dlib
import numpy as np
import multiprocessing
from scipy.spatial import distance as dist
from collections import OrderedDict

from ObjectDetect import ObjectItem

class ObjectTrack():
   
    def __init__(self, frames_till_forget=10, max_distance=50,  
                    tracker_limit=4, min_track_corr=20, debug=False):
        # params
        self.debug              = debug
        self.tracker_limit      = tracker_limit
        self.frames_till_forget = frames_till_forget
        self.max_distance       = max_distance
        self.min_corr           = min_track_corr

        # init
        #TODO muliprocessing
        #self.in_track_queues  = []
        #self.out_track_queues = []

        # id and objects we are keeping track of
        self.next_id     = 0
        self.trackers    = OrderedDict()
        self.objects     = OrderedDict()
        self.disappeared = OrderedDict()

    '''
     This section is where we update the tracks with the image
      and update the object ids
    '''
    def update_objects(self, objs, rgb):
        trk     = []

        # filter for relevant objects
        for obj in objs:
            if(obj.track):
                trk.append(obj)

        trk_ass = self.update(trk, rgb)

        return list(self.objects.values()), trk_ass

    def update_frame(self, rgb, alt=False):
        objs  = []
        derid = []
      
        # loop through trackers and objects then update position 
        for i, (track, obj) in self.trackers.items():
            corr = track.update(rgb)

            # deregister if we are not well corrilated
            if(corr > self.min_corr):
                pos = track.get_position()
                obj.bbox[0] = int(pos.left())
                obj.bbox[1] = int(pos.top())
                obj.bbox[2] = int(pos.right())
                obj.bbox[3] = int(pos.bottom())
        
                cX = int((obj.bbox[0] + obj.bbox[2]) / 2.0)
                cY = int((obj.bbox[1] + obj.bbox[3]) / 2.0)

                self.objects[obj.uid].bbox     = obj.bbox
                self.objects[obj.uid].centroid = (cX, cY)
                
            else:
                derid.append(obj.uid)

        # deregister (can't remove during rotation)
        for oid in derid:
            self.deregister(oid)

        return list(self.objects.values())

    def update_all(self, objs, rgb):

        # run object association
        (u_objs, trk_ass) = self.update_objects(objs, rgb)

        # if the ids of the associations equal the tracked
        #  object ids then there is no need to run the tracker update
        trk_ids  = list(self.objects.keys())
        id_check = all(anid in trk_ids for anid in trk_ass)

        if(id_check):
            u_objs = self.update_frame(rgb, True)

        return u_objs

                                
    '''
     This section of functions handles the association and 
        creation of ids
    '''
    def register(self, obj, cent, rgb):

        # start up a tracker
        amt_trks = len(self.trackers)
        if(amt_trks < self.tracker_limit):
            # find available trk ids
            all_ids   = np.arange(self.tracker_limit).tolist() #TODO probably a better way
            trk_ids   = [oo.tid for oo in list(self.objects.values())]
            avail_ids = list(set(all_ids) - set(trk_ids))

            # update object with id information
            obj.update(self.next_id, cent, avail_ids[0])

            # start up tracker
            t    = dlib.correlation_tracker()
            rect = dlib.rectangle(obj.bbox[0], obj.bbox[1], 
                                    obj.bbox[2], obj.bbox[3])
            t.start_track(rgb, rect)

            # save off
            self.objects[self.next_id]     = obj
            self.disappeared[self.next_id] = 0
            self.trackers[self.next_id]    = (t, obj)

            # get next id
            self.next_id += 1

    def deregister(self, oid):
        del self.objects[oid]
        del self.disappeared[oid]
        del self.trackers[oid]

    def update(self, objs, rgb):
        trk_ass = []

        # if empty no association requiered
        if(len(objs) == 0):

            # mark objects as missing a detection
            oids_to_remove = []
            for oid in self.disappeared.keys():
                self.disappeared[oid] += 1

                # remove if missing for too long
                if(self.disappeared[oid] > self.frames_till_forget):
                    oids_to_remove.append(oid)

            for oid in oids_to_remove:
                self.deregister(oid)

            return trk_ass

        # calculate centroids
        in_centroids = np.array([x.centroid for x in objs])

        # register centroids if nothing is currently being tracked
        if(len(self.objects) == 0):
            for i in range(len(in_centroids)):
                self.register(objs[i], in_centroids[i], rgb)

        else:
            object_ids = list(self.objects.keys())
            tobjs      = list(self.objects.values())
            object_cen = np.array([x.centroid for x in tobjs])

            # calculate distances between tracked objects and new centroids
            dc = dist.cdist(object_cen, in_centroids)
            
            # order by the smallest distances
            rows = dc.min(axis=1).argsort()
            cols = dc.argmin(axis=1)[rows]

            used_rows = set()
            used_cols = set()

            # loop over the distances and look for tracked objects 
            # being close to new centroids
            for (row, col) in zip(rows, cols):
                if((row in used_rows) or (col in used_cols)):
                    continue
               
                # if the distance is under a certain amount we associate
                # the centroid with the object
                if(dc[row,col] < self.max_distance):
                    object_id = object_ids[row]

                    # update
                    self.objects[object_id].centroid = in_centroids[col]
                    self.objects[object_id].bbox     = objs[col].bbox
                    self.disappeared[object_id]      = 0

                    # count number of associations
                    trk_ass.append(object_id)

                    # mark row and col as used
                    used_rows.add(row)
                    used_cols.add(col)

            # which ones are we missing
            unused_rows = set(range(dc.shape[0])).difference(used_rows)
            unused_cols = set(range(dc.shape[1])).difference(used_cols)

            # check for objects who disappeared
            if(dc.shape[0] >= dc.shape[1]):
                for row in unused_rows:
                    object_id = object_ids[row]
                    self.disappeared[object_id] += 1

                    if(self.disappeared[object_id] > self.frames_till_forget):
                        self.deregister(object_id)

            # call it a new object
            else:
                for col in unused_cols:
                    self.register(objs[col], in_centroids[col], rgb)

        return trk_ass


"""
# for multi processing later #TODO
def start_tracker(obj, rgb, input_q, output_q):
    # get tracker from dlib
    t    = dlib.correlation_tracker()
    rect = dlib.rectangle(obj.bbox[0], obj.bbox[1], obj.bbox[2], obj.bbox[3])

    t.start_track(rgb, rect)

    # keep looking for frame updates
    while(True):
        rgb = input_q.get()
        
        # get and update position
        if(rgb is not None):
            t.update(rgb)
    
            pos = t.get_position()
            obj.bbox[0] = int(pos.left()) 
            obj.bbox[1] = int(pos.top())
            obj.bbox[2] = int(pos.right())
            obj.bbox[3] = int(pos.bottom())

            output_q.put(obj)
"""
