'''
 implementation modified from:
 Jean-Paul van Brakel via:
 https://stackoverflow.com/questions/22583391/peak-signal-detection-in-realtime-timeseries-data/22640362#22640362
'''
import numpy as np

# only supports saving to a file
# otherwise we get GTK conflicts with openCV
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt

class Zscore():
    def __init__(self, lag, threshold, influence, memory_size=1):

        self.lag       = lag
        self.threshold = threshold
        self.influence = influence
        self.debug     = False

        self.init       = [False] * memory_size
        self.filtered   = np.zeros(shape=(memory_size, self.lag))
        self.avg_filter = np.zeros(shape=(memory_size, 1))
        self.std_filter = np.zeros(shape=(memory_size, 1))

    # give y_lag as size lag
    def run(self, y_lag, mbank):

        # init algorithm
        if(self.init[mbank]):
            self.filtered[mbank,:] = y_lag
            self.avg_filter[mbank] = np.mean(self.filtered[mbank,:])
            self.std_filter[mbank] = np.std(self.filtered[mbank,:])
            self.init[mbank]       = True

        # setup
        found = None
        yy    = y_lag[-1]

        # did we excede std deviation threshold
        if(np.abs(yy - self.avg_filter[mbank]) > 
                (self.threshold * self.std_filter[mbank])):

            # which way are we over
            if(yy > self.avg_filter[mbank]):
               found = 1 
            else:
               found = -1

            # shift
            fyy = (self.influence * yy) + \
                    ((1-self.influence) * self.filtered[mbank, -1])
            self.filtered[mbank, :] = np.append(
                        self.filtered[mbank, 1:], fyy)

        else:
            self.filtered[mbank, :] = np.append(
                        self.filtered[mbank, 1:], yy)
        
        # update
        self.avg_filter[mbank] = np.mean(self.filtered[mbank,:])
        self.std_filter[mbank] = np.std(self.filtered[mbank,:])

        if(self.debug):
            print('in: {0}, avg/std: {1}/{2}, found: {3}'.format(yy, self.avg_filter[mbank], self.std_filter[mbank], found))
        
        return found

    def plot(self, title, y, mbank):
        fig, axs  = plt.subplots(2, 1)

        axs[0].plot(y, color="b", linewidth=1.0)
        axs[0].axhline(y=self.avg_filter[mbank], color="c", linewidth=2.0)
        axs[0].axhline(y=self.avg_filter[mbank] + (self.threshold * self.std_filter[mbank]),
                    color="g", linewidth=1.0)
        axs[0].axhline(y=self.avg_filter[mbank] - (self.threshold * self.std_filter[mbank]), 
                    color="g", linewidth=1.0)
        axs[0].set_xlabel('frames')
        axs[0].set_title(title)

        fig.tight_layout()
        plt.show()

