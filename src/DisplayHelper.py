"""
    This class will host the camera, window and video recording information
"""
# std
import os
import sys
import time
import argparse
import datetime
import csv
import threading

# available in pip
import numpy as np
import cv2 as cv


class DisplayHelper():
    def __init__(self, rec_only=False):
        # settings
        self.window_name = 'ddd'
        self.width       = 800
        self.height      = 600
        self.vid_fps     = 30.0
        self.scn_debug   = True
        self.time_last   = time.time()
        self.fps         = 0
        self.rec_only    = rec_only

        if(self.rec_only):
            self.vid_codac   = cv.VideoWriter_fourcc(*'XVID')
        else:
            self.vid_codac   = cv.VideoWriter_fourcc(*'MJPG')

        # overlay font
        self.font       = cv.FONT_HERSHEY_SIMPLEX
        self.font_scale = 0.5
        self.font_thick = 1
        self.font_line  = cv.LINE_AA
        self.text_color = (255, 255, 153) # light yellow
        self.back_color = (0, 0, 0)       # black

        # time stamp
        time_str = '{:%Y-%b-%d_%H_%M_%S_%f}'.format(datetime.datetime.now())

        # setup camera
        cap = cv.VideoCapture(0)

        cap.set(cv.CAP_PROP_FRAME_WIDTH,  self.width)
        cap.set(cv.CAP_PROP_FRAME_HEIGHT, self.height)
        cap.set(cv.CAP_PROP_FPS, self.vid_fps)
        cap.set(cv.CAP_PROP_AUTOFOCUS, 0) # off 
        cap.set(cv.CAP_PROP_FOCUS, 0)     # min (255 is max)
        self.__cam    = cap
        self.__raw    = None
        (self.tr_ret, self.tr_image) = self.__cam.read()

        self.thread_running = True
        threading.Thread(target=self.update, args=()).start()

        # setup display
        cv.namedWindow(self.window_name, cv.WINDOW_AUTOSIZE)
        cv.moveWindow(self.window_name, 0, 0)
        cv.setWindowProperty(self.window_name, cv.WND_PROP_FULLSCREEN, 
                                    cv.WINDOW_FULLSCREEN)

        # setup recording streams
        if(not self.rec_only):
            self.outa = cv.VideoWriter('{0}_anno_output.avi'.format(time_str),
                                self.vid_codac, self.vid_fps, 
                                (self.width, self.height))

        self.outr = cv.VideoWriter('{0}_raw_output.avi'.format(time_str),
                            self.vid_codac, self.vid_fps, 
                            (self.width, self.height))

    @property
    def close(self):
        #is_closed = (cv.getWindowProperty(self.window_name, 0) < 0)
        is_closed = (cv.waitKey(1) & 0xFF == ord('q'))
       
        if(is_closed):
            # stop thread
            self.thread_running = False

            # release and destroy
            self.outr.release()
            if(not self.rec_only): self.outa.release()
            self.__cam.release()
            cv.destroyAllWindows()

        
        return is_closed

    def update(self):
        while(True):
            # check for stopped thread
            if(not self.thread_running):
                return

            # grab next frame
            (self.tr_ret, self.tr_image) = self.__cam.read()
            

    def read(self):
        if(self.tr_ret): 
            # camera is mounted upside-down
            img = cv.flip(self.tr_image, -1) 
            rgb = cv.cvtColor(img, cv.COLOR_BGR2RGB)

            # unedited copy for playback
            self.__raw = img.copy()

            # calculate FPS
            now      = time.time()
            self.fps = 1 / (now - self.time_last)
            self.time_last = now
        else: 
            img = None

        return img, rgb

    def display(self, image):
        # add debug information
        if(self.scn_debug):
            fps_info = 'FPS: {:.2f}'.format(self.fps)

            # outer box
            text_start = (15, 15)
            text_size, base = cv.getTextSize(fps_info, self.font, 
                                             self.font_scale, 
                                             self.font_thick)
            text_end  = (text_start[0] + text_size[0], 
                          text_start[1] - text_size[1])
            cv.rectangle(image, 
                            text_start, 
                            text_end,
                            self.back_color, cv.FILLED)
            # FPS
            cv.putText(image, fps_info, text_start, 
                            self.font, self.font_scale, 
                            self.text_color, self.font_thick, 
                            self.font_line)

        # display image
        cv.imshow(self.window_name, image)

        # record video
        self.outr.write(self.__raw)
        if(not self.rec_only): self.outa.write(image)

        
