#!/usr/bin/env python3

"""
    The real time (threaded) version of the DDD pipeline

    #import pdb; pdb.set_trace() #DEBUG
"""

# std
import os
import sys
import time
import argparse
import datetime
import csv
import queue 
import threading

# available in pip
import numpy as np
import cv2 as cv
from termcolor import cprint
from pyfiglet import figlet_format

# own code
from ddd import DDD
from DisplayHelper import DisplayHelper 

def main():     
    # parse command line arguments
    parser = argparse.ArgumentParser()

    # arguments 
    parser.add_argument('-r',  '--rec_only',  action='store_true', 
                            help='Store only raw video with no annotations') 

    parser.add_argument('-d',  '--debug',  action='store_true', 
                            help='Print extra debug information') 

    args = parser.parse_args()

    # welcome text
    cprint(figlet_format('DDD !!!', font='starwars'),
            'yellow', 'on_grey', attrs=['bold'])

    # setup objects
    dh  = DisplayHelper(rec_only=args.rec_only) #t0
    drt = DDD()

    # start text
    cprint(figlet_format('Start', font='cybermedium'),
            'green', 'on_grey', attrs=['bold'])

    if(not args.rec_only):
        # setup thread 1 
        t1_in_q  = queue.Queue(maxsize=1)
        t1_out_q = queue.Queue(maxsize=1)

        # launch t1
        t1 = threading.Thread(target=drt.detect_objects.run_tr, 
                                args=(t1_in_q, t1_out_q), daemon=True)
        t1.start()
    else:
        # rec only text
        cprint(figlet_format('REC Only...', font='cybermedium'),
                'blue', 'on_grey', attrs=['bold'])
   
    # video loop
    while(True):
        # get, display and run detector
        image, rgb = dh.read()

        if(not args.rec_only):
            # pass to queue 
            try:
                t1_in_q.put_nowait(rgb)
            except queue.Full:
                # drop current image if full
                pass

            # pull from queue
            try:
                detections = t1_out_q.get_nowait()
                tracks     = drt.tracker.update_all(detections, rgb)
            except queue.Empty:
                detections = None
                tracks     = None

            # annotate image
            drt.annotate_display(image, detections, tracks, True)

        # display
        dh.display(image)

        # check for exit
        if dh.close:
            break


    # end text
    cprint(figlet_format('FIN', font='cybermedium'),
            'red', 'on_grey', attrs=['bold'])


if __name__ == '__main__':     
	main()
