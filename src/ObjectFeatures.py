'''
    Keeping track of features thoughout run
    and a helper class to handle array expansion
'''
import numpy as np


'''
    automatically handle buffer expansion
'''
class HistArray():
    def __init__(self, rows, dtype=np.float32, keep=True, buffer_size=(3*60*30)):
        self.array = None
        self.rows  = rows
        self.dtype = dtype
        self.keep  = keep
        self.clip  = 0

        # default 3 minutes by 60 seconds by 30 fps
        self.buffer_size = buffer_size

    def update(self, row, idx, val):
        # manage size
        if(self.array is None):
            self.array = np.zeros(shape=(self.rows, self.buffer_size), 
                                    dtype=self.dtype) * np.nan
        else:
            # do we need to expand in time
            if(self.array.shape[1] < (idx+1)):
                if(self.keep):
                    bf         = np.zeros(shape=(self.rows, self.buffer_size), 
                                            dtype=self.dtype) * np.nan
                    self.array = np.append(self.array, bf, axis=1)

                else:
                    self.clip = idx

                    # keep the last half and add new buffer
                    bf         = np.zeros(shape=(self.rows, (self.buffer_size // 2)), 
                                            dtype=self.dtype) * np.nan
                    self.array = np.append(self.array[:,:(self.array.shape[1] // 2)], bf, axis=1)

        # add value
        if(self.keep):
            self.array[row, idx] = val
        else:
            self.array[row, idx - self.clip] = val

'''
    organize all features by frame
'''
class ObjFeatures():
    def __init__(self, max_track, is_live=False, buffer_size=(3*60*30)):
        # settings
        self.max_track  = max_track
        self.is_live    = is_live

        # smoothing
        self.smooth_win = 8
        win             = np.hanning(self.smooth_win)
        self.norm_win   = win/win.sum()

        # values
        self.feature_list = ["area", "pos_w", "pos_h"]

        self.uid_key  = HistArray(rows=max_track, keep=(not is_live), buffer_size=buffer_size)

        self.feat  = {}
        self.sfeat = {}
        self.dfeat = {}
        for fkey in self.feature_list:
            self.feat[fkey]  = HistArray(rows=max_track, keep=(not is_live), buffer_size=buffer_size)
            self.sfeat[fkey] = HistArray(rows=max_track, keep=(not is_live), buffer_size=buffer_size)
            self.dfeat[fkey] = HistArray(rows=max_track, keep=(not is_live), buffer_size=buffer_size)


    def update(self, uid, tid, fcnt, area, dis):
        # add values
        self.uid_key.update(tid, fcnt, uid)
        self.feat['area'].update(tid, fcnt, area)
        self.feat['pos_w'].update(tid, fcnt, dis[0])
        self.feat['pos_h'].update(tid, fcnt, dis[1])

        for key, ft in self.feat.items():
            for i in range(ft.array.shape[0]):

                if(fcnt > self.smooth_win):
                    # smooth values
                    weights = self.norm_win * ft.array[i, (fcnt - self.smooth_win):fcnt]
                    smooth  = np.sum(weights)
                    # note this is delayed by self.smooth_win // 2
                    self.sfeat[key].update(i, fcnt, smooth)

                    # take derivative
                    if(fcnt == self.smooth_win):
                        self.dfeat[key].update(i, fcnt, smooth)
                    else:
                        last = self.sfeat[key].array[i,fcnt-1]
                        self.dfeat[key].update(i, fcnt, smooth - last)
                        

    def gen_features(self, objs, frame_count):
       for obj in objs:
           if(obj.tracked):
               # create features
               area = (obj.bbox[2] - obj.bbox[0]) * \
                       (obj.bbox[3] - obj.bbox[1])

               self.update(obj.uid, obj.tid,
                            frame_count, 
                            area, obj.centroid)
