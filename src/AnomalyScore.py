#!/usr/bin/env python3
'''
    Anomaly score with signal processing
'''
#default
import copy
import os
import argparse

# from pip
import numpy as np
import pandas as pd

# only supports saving to a file
# otherwise we get GTK conflicts with openCV
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt

# from this
from Zscore import Zscore
from ObjectFeatures import ObjFeatures, HistArray

class AnomalyScore():
    def __init__(self, params, feature_list, max_track, is_live=False, simple=False):
        self.is_live = is_live
        self.simple  = simple

        # z-score
        self.z_score = {}
        self.z_score['norm'] = Zscore(lag = params['zscore']['norm']['lag'],
                                threshold = params['zscore']['norm']['threshold'],
                                influence = params['zscore']['norm']['influence'],
                                memory_size = max_track * len(feature_list))

        self.z_score['diff'] = Zscore(lag = params['zscore']['diff']['lag'],
                                threshold = params['zscore']['diff']['threshold'],
                                influence = params['zscore']['diff']['influence'],
                                memory_size = max_track * len(feature_list))
        # simple
        self.s_window = 60
        self.s_std    = 1.8

        # FFT
        self.fft_win     = params['fft']['window']
        self.fft_bins    = 20
        self.fft_Fs      = 1
        self.fft_range   = 0.2
        self.fft_nan_max = 0.4
        self.fft_lim     = 10

        # distraction scoring
        self.score = HistArray(rows=max_track, 
                               dtype=np.uint8)
        self.score_feat = params['score']

        # gate anything that adds a score with a min peak time
        self.anom_min_frame = params['peak']['min_frame']
        self.peak_counter   = np.zeros(shape=(max_track, len(self.score_feat.keys()))) 

        # plotting help
        self.peaks= {}
        for fkey in feature_list:
            self.peaks[fkey] = HistArray(rows=max_track, 
                                         keep=(not self.is_live))

    def sig_detect(self, features, fcnt, tune=False):
        """ Process signal and save off score
        """
        score_total = np.zeros(features.max_track)
    
        # look through all smooth features
        for i, (key, ft) in enumerate(features.sfeat.items()):
            if(ft.array is not None):
                # rotate through track
                for trk_idx in range(ft.array.shape[0]):

                    #TODO find a way to integrate this into the normal score
                    # this is sub-sampled attempting to find jumps in trends
                    if(self.simple and fcnt > self.s_window):

                        # anomaly detection
                        s = ft.array[trk_idx, fcnt-self.s_window:fcnt]

                        # resample
                        s = s[::10]

                        mean = np.mean(s)
                        std  = np.std(s)

                        over  = s[-1] > (mean + self.s_std*std)
                        under = s[-1] < (mean - self.s_std*std)

                        if(over or under):
                            self.peaks[key].update(trk_idx, fcnt, 1)
                            score_total[trk_idx] = score_total[trk_idx] + self.score_feat[key]

                        # oscillation detection
                        # find FFT; only for pos_w
                        if(key == "pos_w"):
                            s    = ft.array[trk_idx, :]
                            hist = self.an_fft(fcnt, s)

                            # score is a fraction of the displaced energy
                            if(hist is not None):
                                score_total[trk_idx] = score_total[trk_idx] + \
                                    ((np.sum(hist[2:5]) / np.sum(hist)) * \
                                        self.score_feat['pos_w_fft'])

                    else:
                        # index calc for z_score
                        z_index = i
                        if(ft.array.shape[0] > 1):
                            z_index = (i + (trk_idx + i))

                        # find peaks 
                        if(fcnt > np.max([self.z_score['norm'].lag, self.z_score['diff'].lag])):
                            found = None
                            s     = None

                            if(key == "pos_w"):
                                s = ft.array[trk_idx, fcnt-self.z_score['norm'].lag:fcnt]
                                found = self.z_score['norm'].run(s, z_index)

                            elif((key == "pos_h") or (key == "area")):
                                s = features.dfeat[key].array[trk_idx, fcnt-self.z_score['diff'].lag:fcnt]
                                found = self.z_score['diff'].run(s, z_index)
                            
                            if(found is not None):
                                self.peak_counter[trk_idx, i] = self.peak_counter[trk_idx, i] + 1
                                if(self.peak_counter[trk_idx, i] >= self.anom_min_frame):
                                    self.peaks[key].update(trk_idx, fcnt, 1)
                                    score_total[trk_idx] = score_total[trk_idx] + self.score_feat[key]

                            else:
                                self.peak_counter[trk_idx, i] = 0


                            # print tunning plots
                            if(tune and
                               (s is not None) and
                               (fcnt > self.fft_win) and 
                               (fcnt % (self.fft_win // 4) == 0)):

                                title = '(f: {3}, key: {0}, trk: {1}) = {2}'.format(key, trk_idx, found, fcnt)

                                if(key == "pos_w"):
                                    self.z_score['norm'].plot(title, s, z_index)

                                elif((key == "pos_h") or (key == "area")):
                                    self.z_score['diff'].plot(title, s, z_index)

                        # find FFT, only for pos_w
                        if(key == "pos_w"):
                             # only run fft every windown/4 frame 
                             if((fcnt > self.fft_win) and 
                                 (fcnt % (self.fft_win // 2) == 0)):

                                s    = ft.array[trk_idx, :]
                                hist = self.an_fft(fcnt, s, tune)

                                # score is based on fft small bin ratio
                                if(hist is not None):
                                    fft_score = (np.sum(hist[1:4]) / np.sum(hist)) * \
                                            self.score_feat['{0}_fft'.format(key)]

                                    score_total[trk_idx] = score_total[trk_idx] + fft_score

                                    # allow the score to impact past values
                                    score_window = (fcnt - (self.fft_win // 2))
                                    self.score.array[trk_idx, score_window:(fcnt-1)] = fft_score + \
                                         self.score.array[trk_idx, score_window:(fcnt-1)]


        # add up score and flag uids
        if(features.uid_key.array is not None):
            for i, score in enumerate(score_total):
                self.score.update(i, fcnt, score)

        return True
        
    def an_fft(self, fcnt, signal, tune=False):
        s = signal[fcnt-self.fft_win:fcnt]

        # if we have over (nan_max)% NaNs don't run FFT
        mask = np.isfinite(s)
        if((np.sum(mask) / self.fft_win) > self.fft_nan_max):

            # linear interpolate NaNs
            si       = np.arange(len(s))
            s_interp = np.interp(si, si[mask], s[mask])

            # shift mean to 0
            s_interp = s_interp - np.mean(s_interp)

            # FFT 
            fft = np.fft.hfft(s_interp)

            # get positive half and normalize by sample amount
            hist, bin_edges = np.histogram(np.abs(fft)[:self.fft_win // 2] * 1 / self.fft_win, 
                                            bins=self.fft_bins, range=(0,self.fft_range))

            # print tunning plots
            if(tune):
                print(hist)
                print(bin_edges)
                fig, axs = plt.subplots(2, 1)
                axs[0].set_ylabel("Amplitude")
                axs[0].set_xlabel("Frequency [Hz]")
                N = self.fft_win
                f = np.linspace(0, self.fft_Fs, self.fft_win)
                axs[0].bar(bin_edges[:-1], hist, color='c', edgecolor='b', 
                                width=self.fft_range/self.fft_bins)
                axs[0].axhline(y=self.fft_lim, color='r', linestyle='--')
                axs[0].set_ylim([0, 70])

                axs[1].set_xlabel("Frames")
                axs[1].set_ylabel("Position [pixels]")
                axs[1].set_ylim([-100, 100])
                axs[1].plot(s_interp)
                fig.tight_layout()
                plt.savefig('fft_tune.png', dpi=300)
                input("Press Enter...")
                plt.show()

            return hist


    
    #TODO might want to remove this
    def update(self, features, fcnt, tune=False):
        self.sig_detect(features, fcnt, tune)


    def plot(self, prefix, features, frame_end):

        ###
        # FIGURE 0
        ###
        if(self.score.array is not None):
            # score
            trk_amt   = self.score.array.shape[0]
            fig, axs  = plt.subplots(trk_amt, 1)
            max_score = np.sum(list(self.score_feat.values()))

            if(trk_amt > 1):
                for i in range(trk_amt):
                    y = self.score.array[i, :frame_end]
                    axs[i].plot(y, color='b', label='score')
                    #
                    axs[i].set_ylim([0, max_score])
                    axs[i].set_xlim([0, frame_end])
                    axs[i].set_ylabel('Score')
                    axs[i].set_xlabel('Frames')
                    #
                    major_ticks = np.arange(0, y.shape[0]+1, 100)
                    minor_ticks = np.arange(0, y.shape[0]+1, 20)
                    axs[i].set_xticks(major_ticks)
                    axs[i].set_xticks(minor_ticks, minor=True)
                    major_ticks = np.arange(0, max_score+1, 1)
                    minor_ticks = np.arange(0, max_score+1, 0.2)
                    axs[i].set_yticks(major_ticks)
                    axs[i].set_yticks(minor_ticks, minor=True)
                    axs[i].grid(which='minor', alpha=0.2)
                    axs[i].grid(which='major', alpha=0.5)
                    #
                    axs[i].legend(loc="upper right")
            else:
                y = self.score.array[0, :frame_end]
                axs.plot(y, color='b', label='score')
                axs.set_ylim([0, max_score])
                axs.set_xlim([0, frame_end])
                #
                major_ticks = np.arange(0, y.shape[0]+1, 100)
                minor_ticks = np.arange(0, y.shape[0]+1, 20)
                axs.set_xticks(major_ticks)
                axs.set_xticks(minor_ticks, minor=True)
                major_ticks = np.arange(0, max_score+1, 1)
                minor_ticks = np.arange(0, max_score+1, 0.2)
                axs.set_yticks(major_ticks)
                axs.set_yticks(minor_ticks, minor=True)
                axs.grid(which='minor', alpha=0.2)
                axs.grid(which='major', alpha=0.5)
                #
                axs.set_ylabel('Score')
                axs.set_xlabel('Frames')
                axs.legend(loc="upper right")
                
            fig.tight_layout()
            plt.savefig('{0}/score.png'.format(prefix), dpi=300)
            plt.close()

        # only plot score if live
        # otherwise post processes for other plots
        if(self.is_live):
            return

        # signal and peak information
        for key, ft in features.feat.items():
            if(ft.array is None):
                continue
            trk_amt  = ft.array.shape[0]

            ###
            # FIGURE 1
            ###
            for i in range(trk_amt):
                fig, axs = plt.subplots(2, 1)

                # plot raw and smooth data
                # shift raw to match smooth delay
                raw = np.append(np.zeros(features.smooth_win // 2), ft.array[i,:frame_end])
                axs[0].plot(raw, c='b', label='raw',linewidth=1.0)
                axs[0].plot(features.sfeat[key].array[i,:frame_end], c='r', 
                                label='smooth',linewidth=1.0)

                # plot peak information
                if(self.peaks[key].array is not None):
                    peaks    = self.peaks[key].array[i,:frame_end]
                    y_values = features.sfeat[key].array[i,:frame_end] * peaks
                    axs[0].plot(y_values, marker='+', markersize=5, c='g', label='peaks')

                # settings
                if(key == "area"):
                    axs[0].set_ylabel('Position [pixels$^2$]'.format(key))
                else:
                    axs[0].set_ylabel('Position [pixels]'.format(key))
                axs[0].set_xlabel('Frames')
                axs[0].set_xlim([0, frame_end])
                axs[0].grid(True)
                axs[0].legend(loc="upper right")

                # diff data
                axs[1].plot(features.dfeat[key].array[i,:frame_end], c='c', 
                                label='d/df',linewidth=1.0)

                # peak diff data
                if(self.peaks[key].array is not None):
                    peaks    = self.peaks[key].array[i,:frame_end]
                    y_values = features.dfeat[key].array[i,:frame_end] * peaks
                    axs[1].plot(y_values, marker='+', markersize=5, c='g', label='peaks')

                # settings
                if(key == "area"):
                    axs[1].set_ylabel('Difference [pixels$^2$]'.format(key))
                else:
                    axs[1].set_ylabel('Difference [pixels]'.format(key))
                axs[1].set_xlabel('Frames')
                axs[1].set_xlim([0, frame_end])
                axs[1].grid(True)
                axs[1].legend(loc="upper right")
            
                fig.tight_layout()
                plt.savefig('{1}/{2}_trk_{0}_val.png'.format(key, prefix, i), dpi=500)
                plt.close()

    '''
        Debug and extra functions
    '''
    def tune_plot(self, df):
        sig   = df.loc[:, 'pos_w'].values
        means = np.zeros_like(sig)
        stds  = np.zeros_like(sig)

        # loop
        for i in range(sig.shape[0]):
            if(i > self.s_window):
                s        = sig[i-self.s_window:i]
                means[i] = np.mean(s)
                stds[i]  = np.std(s)

        # plot
        plt.figure(1)
        plt.title('mean and std')
        plt.plot(sig, c='g', label='signal')
        plt.plot(means, c='b', label='mean')

        plt.plot(means + (self.s_std * stds), c='r', label='upper')
        plt.plot(means - (self.s_std * stds), c='r', label='lower')
        plt.legend()
        plt.savefig('./tune.png', dpi=500)
        plt.close()
        

    def debug_run(self, pkl_filepath, tune_only=False):
        df   = pd.read_pickle(pkl_filepath)
        ittr = len(df.index)

        if(tune_only):
            self.tune_plot(df)
            return

        # only import one track at a time
        features = ObjFeatures(max_track=1, 
                                is_live=False, 
                                buffer_size=ittr)

        # act on features as if running live
        for index, row in df.iterrows():
            features.update(row['uid'], 0, index, 
                            row['area'], (row['pos_w'], row['pos_h']))

            # run score 
            self.sig_detect(features, index)
        

        # save off features with updated new score
        prefix_og = './rerun_anom'
        prefix    = prefix_og
        i         = 0
        while(os.path.exists(prefix)):
            prefix = prefix_og + '_{0}'.format(i)
            i      = i + 1

        os.mkdir(prefix)
        self.plot(prefix, features, ittr)
        self.save(prefix, features, ittr)
        

    def save(self, prefix, features, frame_end):
        # convert all numpy arrays to pandas data frame per track 
        for i in range(features.max_track):
            # setup initial dataframe info
            df = pd.DataFrame({'uid':   features.uid_key.array[i, :frame_end],
                               'score': self.score.array[i, :frame_end]})

            # add features dynamically
            for fkey in features.feature_list:
                df[fkey] = features.feat[fkey].array[i, :frame_end]

            # save
            df.to_pickle('{0}/trk{1}_feat.pkl'.format(prefix, i))

def main():     
    # parse command line arguments
    parser = argparse.ArgumentParser()

    # arguments
    parser.add_argument('-p',  '--rerun', 
                            help='Load feature data frame (provide path)') 

    args = parser.parse_args()

    # setup objects
    features = ObjFeatures(max_track=1) #TODO parameters
    anomaly  = AnomalyScore(None, feature_list=features.feature_list,
                                                max_track=1)
    # run
    if(args.rerun is not None):
        anomaly.debug_run(args.rerun)


if __name__ == '__main__':     
	main()
