'''
    Wrapper for object detection
'''
import csv
import cv2 
import time
import queue
import numpy as np

from edgetpu.detection.engine import DetectionEngine
from PIL import Image

from AnomalyScore import ObjFeatures

class ObjectItem():
    def __init__(self, bounding_box, label, score):
        self.bbox  = bounding_box
        self.label = label
        self.score = score

        # only track certain things
        # with a certain confidence
        if(((label == "car") or
           (label == "truck") or
           (label == "train")) and
           (score > 0.40)):
            self.track = True
        else:
            self.track = False

        # calculate centroid
        if(self.track):
            sX = self.bbox[0] 
            sY = self.bbox[1]
            eX = self.bbox[2]
            eY = self.bbox[3]
        
            cX = int((sX + eX) / 2.0)
            cY = int((sY + eY) / 2.0)

            self.centroid = (cX, cY)

        else:
            self.centroid = (0, 0)

        # focus is True when this box is directly
        # in front of us
        self.focus    = False

        # set true if being tracked
        self.tracked  = False
        self.uid      = 0
        self.tid      = 0

        # behavior
        self.abnormal   = False
        self.distracted = False

    def update(self, uid, cent, tid):
        self.tracked  = True
        self.uid      = uid
        self.tid      = tid
        self.centroid = cent

class ObjectDetect():
    
    def __init__(self, w, h, min_thresh=0.45, max_det=30, debug=False):

        self.labels_fp  = '../models/coco_labels.txt'
        self.network_fp = '../models/mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite'
        self.ids_to_keep_fp = '../models/coco_keep.csv'

        self.debug      = debug
        self.bbox_max_w = 0.9 * w
        self.bbox_max_h = 0.9 * h
        self.min_thresh = min_thresh
        self.max_det    = max_det

        # read in labels for detector
        with open(self.labels_fp, 'r', encoding="utf-8") as f:
            lines = f.readlines()
        self.labels = {}
        for line in lines:
            pair = line.strip().split(maxsplit=1)
            self.labels[int(pair[0])] = pair[1].strip()

        # read in labels to keep
        self.ids_to_keep = []
        with open(self.ids_to_keep_fp, 'r') as csvFile:
            for row in csv.reader(csvFile):
                self.ids_to_keep.append(int(row[0]))

        # initialize engine
        self.ssd_engine = DetectionEngine(self.network_fp)

    '''
        img is as an openCV image
        returned filtered object box descriptions
    '''
    def run(self, rgb):
        # convert to PIL format
        im_pil = Image.fromarray(rgb)

        # run pipeline
        obj_raw = self.detect_objects(im_pil)
        obj_det = self.object_processing(obj_raw)

        return obj_det

    def run_tr(self, in_q, out_q):
        while(True):
            # pull from queue
            rgb = in_q.get()

            if(rgb is not None):
                # run detect
                det_objs = self.run(rgb)
                out_q.put(det_objs)

    def detect_objects(self, image):
        # run inference
        obj_det = self.ssd_engine.DetectWithImage(image, 
                                                    threshold=self.min_thresh,
                                                    keep_aspect_ratio=False,
                                                    relative_coord=False, 
                                                    top_k=self.max_det)
        return obj_det

    '''
     Filter objects we don't care about both by
        label and size
    '''
    def object_processing(self, obj_det):
        obj_itm = []

        # only want to loop once
        if obj_det:
            for obj in obj_det:
                # filter by label
                if not (obj.label_id in self.ids_to_keep):
                    continue

                # filter by bbox size 
                box   = obj.bounding_box.astype(int).flatten().tolist()
                box_w = box[2] - box[0]
                box_h = box[3] - box[1]

                if((box_w > self.bbox_max_w) 
                        or (box_h > self.bbox_max_h)):
                    continue

                # add object to list
                item = ObjectItem(box, self.labels[obj.label_id],
                                    obj.score)
                obj_itm.append(item)

        return obj_itm

