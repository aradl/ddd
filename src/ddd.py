#!/usr/bin/env python3
# default
import os
import sys
import time
import argparse
import datetime
import csv
import multiprocessing
import operator
import queue 
import json
import math

# from pip
import yaml
import numpy as np
import pandas as pd
import cv2 as cv
import matplotlib.pyplot as plt

from termcolor import cprint
from pyfiglet import figlet_format
from moviepy.editor import VideoFileClip

from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.utils.multiclass import unique_labels

# from this
from ObjectDetect import ObjectDetect, ObjectItem
from ObjectTrack import ObjectTrack
from AnomalyScore import AnomalyScore, ObjFeatures
from ClusterSeries import ClusterSeries 
from BehaviorEst import BehaviorEst 

#import pdb; pdb.set_trace() #DEBUG

class DDD():

    def __init__(self, parameters, is_live=False, replay=False):
        # target values
        self.img_width  = 800
        self.img_height = 600

        # ddd settings
        self.tracker_limit = 2
        self.frame_counter = 0

        # debug settings
        self.det_en    = True
        self.det_dbg   = False
        self.track_en  = True
        self.track_dbg = False
        self.behv_en   = True
        self.tune      = False

        # annotation settings
        self.font       = cv.FONT_HERSHEY_SIMPLEX
        self.font_scale = 0.4
        self.font_thick = 1
        self.font_line  = cv.LINE_AA

        self.marker       = cv.MARKER_DIAMOND
        self.marker_size  = 8 
        self.marker_thick = 1

        self.main_color   = (0, 255, 0)     # green
        self.second_color = (66, 134, 244)  # blue
        self.third_color  = (102, 102, 153) # grey
        self.text_color   = (255, 255, 153) # light yellow
        self.back_color   = (0, 0, 0)       # black
        self.laneL_color  = (255, 153, 51)  # orange
        self.laneR_color  = (255, 102, 153) # pink
        self.mark_color   = (0, 255, 0)     # green
        self.warn_color   = (255, 255, 0)   # yellow
        self.dist_color   = (255, 0, 0)     # red

        # output
        self.output_image_fpt = ('./', 'result', '.jpg')

        # parameters
        anomaly_params = parameters['AnomalyScore']
        cluster_params = parameters['ClusterSeries']
        behav_params   = parameters['BehaviorEst']

        # objects 
        if(replay):
            # only import one track at a time during replay
            self.tracker_limit = 1

            self.detect_objects = None
            self.tracker        = None
        else:
            self.detect_objects = ObjectDetect(self.img_width, self.img_height,
                                                min_thresh=0.35, max_det=40,
                                                debug=self.det_dbg)

            self.tracker        = ObjectTrack(frames_till_forget=27,
                                                max_distance=50,
                                                tracker_limit=self.tracker_limit,
                                                min_track_corr=9,
                                                debug=self.track_dbg)

        self.features      = ObjFeatures(max_track=self.tracker_limit,
                                           is_live=is_live)

        self.anomaly       = AnomalyScore(anomaly_params,
                                            feature_list=self.features.feature_list,
                                            max_track=self.tracker_limit,
                                            is_live=is_live)

        self.cluster = []
        self.behav   = []
        for i in range(self.tracker_limit):
            self.cluster.append(ClusterSeries(cluster_params, track=i))
            self.behav.append(BehaviorEst(behav_params, track=i))

    def annotate_display(self, image, det_objs, trk_objs, related):
        """ Create annotations for the image based on objects and behavior
        """

        # combine lists if they exists
        objs = None 
        if(trk_objs and det_objs):
            objs = det_objs + trk_objs 
        elif(trk_objs):
            objs = trk_objs
        else:
            objs = det_objs

        # run though all objects
        if(objs):
            for obj in objs:
                anno_str = '{0}: {1:.2f}, {2}'.format(obj.label, obj.score, obj.uid)

                if(self.det_dbg):
                    print('-----------------------------------------')
                    print(anno_str)

                # bounding box color
                if(obj.tracked and obj.focus):
                    box_color = self.main_color
                elif(obj.tracked):
                    box_color = self.second_color 
                else:
                    box_color = self.third_color 

                # bounding box
                if(obj.tracked and (obj.distracted or obj.abnormal)):
                    # double box
                    offset    = 5
                    if(obj.distracted):
                        box_color = self.dist_color
                    elif(obj.abnormal):
                        box_color = self.warn_color

                    cv.rectangle(image, 
                                    (obj.bbox[0], obj.bbox[1]), (obj.bbox[2], obj.bbox[3]),
                                    box_color, 2)

                    cv.rectangle(image, 
                                    (obj.bbox[0] - offset, obj.bbox[1] - offset), 
                                    (obj.bbox[2] + offset, obj.bbox[3] + offset),
                                    box_color, 2)
                else:
                    cv.rectangle(image, 
                                    (obj.bbox[0], obj.bbox[1]), (obj.bbox[2], obj.bbox[3]),
                                    box_color, 2)

                # box annotation back
                text_size, base = cv.getTextSize(anno_str, self.font, 
                                                    self.font_scale, 
                                                    self.font_thick)
                text_point = (obj.bbox[0], obj.bbox[1])
                text_off   = (obj.bbox[0] + text_size[0], 
                                obj.bbox[1] - text_size[1])
                cv.rectangle(image, 
                                text_point, 
                                text_off,
                                self.back_color, cv.FILLED)

                # box annotation
                cv.putText(image, anno_str, text_point, 
                                self.font, self.font_scale, 
                                self.text_color, self.font_thick, 
                                self.font_line)

                # draw centroid if being tracked
                if(obj.tracked):
                    marker_color = self.mark_color
                    if(obj.distracted):
                        marker_color = self.dist_color
                    elif(obj.abnormal):
                        marker_color = self.warn_color

                    cv.drawMarker(image, tuple(obj.centroid), marker_color,
                                    self.marker, self.marker_size, 
                                    self.marker_thick, self.font_line)

        else:
            if(self.det_dbg):
                print('No objects detected!')

        # add frame counter 
        frame_info = 'Frame: {0}'.format(int(self.frame_counter))

        # outer box
        text_start = (5, 15)
        text_size, base = cv.getTextSize(frame_info, self.font, 
                                         self.font_scale, 
                                         self.font_thick)
        text_end  = (text_start[0] + text_size[0], 
                      text_start[1] - text_size[1])
        cv.rectangle(image, 
                        text_start, 
                        text_end,
                        self.back_color, cv.FILLED)
        # frame counter
        cv.putText(image, frame_info, text_start, 
                        self.font, self.font_scale, 
                        self.text_color, self.font_thick, 
                        self.font_line)
        
        return image

    def contex_proc(self, objs):
        """ Attempt to understand which detection is the most important
        """

        # closest to the center along width only
        objs = sorted(objs, key=lambda x: x.centroid[0], reverse=True)

        return objs
        

    def image_pipeline(self, img, related=True):
        """ Run the full pipline of processing techniques on an image
        """ 

        rgb       = cv.cvtColor(img, cv.COLOR_BGR2RGB)
        det_objs  = None
        trk_objs  = None
        beh_infer = {}

        # get detections
        if(self.det_en):
            det_objs = self.detect_objects.run(rgb)
            det_objs = self.contex_proc(det_objs)

        # setup trackers
        if(related and self.track_en):
            trk_objs = self.tracker.update_all(det_objs, rgb)

            if(self.track_dbg):
                print('frame: {0}, trackers: {1}'.format(self.frame_counter, 
                                len(self.tracker.trackers)))
        
        # understand behavior
        if(self.behv_en):
            if(trk_objs):
                self.features.gen_features(trk_objs, self.frame_counter)

                # detect anomolous behavior
                self.anomaly.update(self.features, self.frame_counter, self.tune)

                for i in range(self.tracker.tracker_limit):
                    # learn similar behavior
                    self.cluster[i].update(self.frame_counter, self.features, self.anomaly.score)
                    
                    # infer behavior
                    uid, beh = self.behav[i].update(self.cluster[i].hist_labels.array, 
                                                        self.cluster[i].hist_score.array,
                                                        self.cluster[i].hist_size.array,
                                                        self.anomaly.score.array[i, :],
                                                        self.features.uid_key.array[i, :],
                                                        self.frame_counter)
                    if(uid is not None): beh_infer[uid] = beh
        
        
        # mark objects behavior
        if(trk_objs):
            for obj in trk_objs:
                # check uids
                if(obj.uid in beh_infer.keys()):
                    if(beh_infer[obj.uid] == self.behav[0].state['abnormal']):
                        obj.abnormal   = True
                        obj.distracted = False
                        continue

                    if(beh_infer[obj.uid] == self.behav[0].state['distracted']):
                        obj.distracted = True
                        obj.abnormal   = False
                        continue

                else:
                    obj.abnormal   = False
                    obj.distracted = False

        # draw on image
        if(not self.det_dbg):
            det_objs = []

        img = self.annotate_display(img, det_objs, trk_objs,
                                    related)
        if(related): self.frame_counter += 1
        return img


    def run(self, image_file_path=None, video_file_path=None, debug=False, tune=False):
        # setup flags
        self.track_dbg = debug
        self.det_dbg   = debug
        #
        self.tune  = tune
   
        # start text
        cprint(figlet_format('start', font='cybermedium'),
                'green', 'on_grey', attrs=['bold'])

        # find source type
        if(image_file_path):
            # run on a set of images 
            img  = cv.imread(image_file_path)
            img  = self.image_pipeline(img, False)

            # save output
            time_str  = '{:%Y-%b-%d_%H:%M:%S:%f}'.format(datetime.datetime.now())
            output_fp = '{0}{1}_{2}{3}'.format(self.output_image_fpt[0], 
                                                self.output_image_fpt[1], 
                                                time_str, self.output_image_fpt[2])
            cv.imwrite(output_fp, img)

        elif(video_file_path):
            # create results directory
            results_path = os.path.splitext(video_file_path)[0]

            # if it exists skip video
            if(os.path.exists(results_path)):
                print('NOTE: Video has an existing run in this location (remove if a re-run is needed)')
                return
            os.mkdir(results_path)

            # run on video file
            clip = VideoFileClip(video_file_path).\
                                    fl_image(self.image_pipeline)
            clip.write_videofile('{0}/result.mp4'.format(results_path), 
                                    audio=False)

            # plot some features
            self.anomaly.plot(results_path, self.features, 
                                self.frame_counter)
            self.anomaly.save(results_path, self.features, 
                                self.frame_counter)

            for i in range(self.tracker.tracker_limit):
                self.cluster[i].plot(results_path, self.features, 
                                    self.frame_counter)
                self.behav[i].plot(results_path, self.features, 
                                    self.frame_counter)
        # end text
        cprint(figlet_format('stop', font='cybermedium'),
                'red', 'on_grey', attrs=['bold'])

    def debug_run(self, prefix, pkl_filepath, truth_df=None, skip_plots=False):
        df   = pd.read_pickle(pkl_filepath)
        ittr = len(df.index)

        # act on features as if running live
        for index, row in df.iterrows():
            self.features.update(row['uid'], 0, index, 
                            row['area'], (row['pos_w'], row['pos_h']))

           
            # skip if there are no objects being tracked
            if(~np.isnan(row['uid'])):
                self.anomaly.update(self.features, index, False)

                # learn similar behavior
                self.cluster[0].update(index, self.features, self.anomaly.score)
                
                # infer behavior
                uid, beh = self.behav[0].update(self.cluster[0].hist_labels.array, 
                                                    self.cluster[0].hist_score.array,
                                                    self.cluster[0].hist_size.array,
                                                    self.anomaly.score.array[0, :],
                                                    self.features.uid_key.array[0, :],
                                                    index)

        # generate plots
        if(not skip_plots):
            self.anomaly.plot(prefix, self.features, ittr)
            self.cluster[0].plot(prefix, self.features, ittr)
            self.behav[0].plot(prefix, self.features, ittr)

        pred = None
        if(self.behav[0].get_pred(ittr) is not None):
            pred = self.behav[0].get_pred(ittr)

        # if we have truth generate confusion matrix
        cm = None
        if((truth_df is not None) and (pred is not None)):
            if(not skip_plots):
                cm = self.debug_plot(prefix, pred, truth_df)
            else:
                truth = truth_df['truth'].to_numpy()
                cm    = confusion_matrix(truth, pred)

        return cm, pred 

    def debug_plot(self, prefix, behav_pred, truth_df):
        truth = truth_df['truth'].to_numpy()

        # calculate truth
        _, cm = plot_confusion_matrix(truth, behav_pred, ['normal', 'abnormal'])
        plt.savefig('{0}/cm.png'.format(prefix), dpi=300)
        plt.close()

        return cm

# code from: https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
def plot_confusion_matrix(y_true, y_pred, classes,
                      normalize=True,
                      title=None,
                      cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    #classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax, cm

def auto_batch_test(parameters, prefix, run_folder_path, 
                        truth_file, skip_plots=True, verbose=False):

    # find folders which match standard video nameing format
    sub_folder_list = [x for x in os.listdir(run_folder_path) 
                            if os.path.isdir(os.path.join(run_folder_path, x))]
    run_folder_list = [x for x in sub_folder_list
                            #if((x.startswith('D')) and ('_C' in x))]
                            if(not (x.startswith('_')))]

    # load in json
    with open(truth_file) as json_file:
        truth_labels = json.load(json_file)

    # loop through and run
    report_info = {}
    for dirs in run_folder_list:
        info       = truth_labels[dirs]
        run_folder = os.path.join(run_folder_path, dirs)
        sub_prefix = os.path.join(prefix, dirs)
        os.mkdir(sub_prefix)

        # find associated pickle file from provided directory
        feat_fp = [x for x in os.listdir(run_folder)
                        if(x.startswith('trk') and x.endswith('feat.pkl'))]
        if(len(feat_fp) != 0):
            feat_fp.sort()
            feat_fp = os.path.join(run_folder, feat_fp[info['trk']])
        else:
            feat_fp = None

        # skip if track file is not found
        if(feat_fp is None):
            print('NOTE: files not found for: {} - Track: {}'.format(dirs, feat_fp))
            continue

        # load file and generate truth
        df        = pd.read_pickle(feat_fp)
        frame_end = len(df.index)
        truth     = np.zeros(frame_end, dtype=np.int)

        # mark truth array
        if(info['dis'] is not None):
            for tup in info['dis']:
                truth[tup[0]:tup[1]] = 1
        truth_df = pd.DataFrame({'truth': truth})

        # init and run
        if(verbose): print('Re-run on: {}\n Track: {}'.format(dirs, feat_fp))
        ddd = DDD(parameters, is_live=False, replay=True)
        cm, pred  = ddd.debug_run(sub_prefix, feat_fp, truth_df, skip_plots)
        if(verbose): print(cm)

        # metric calculation
        if((cm is not None) 
                and (len(cm.ravel()) > 1)):

            tn, fp, fn, tp = cm.ravel()
            acc = (tp + tn) / (tp + tn + fp + fn)

            # F2 score
            beta = 2
            f2   = ((1 + beta**2)*tp) / ((1 + beta**2)*tp + (beta**2 * fn) + fp)

            # matthews correlation coefficient
            num = (tp*tn) - (fp*fn)
            den = math.sqrt((tp + fp)*(tp + fn)*(tn + fp)*(tn + fn))
            if(den == 0): den = 1
            mcc = num / den

            # true/false positive rates
            false_positive_rate = fp / (fp + tn)
            true_positive_rate  = tp / (tp + fn)
        else:
            acc = 0
            f2  = 0
            mcc = -1
            false_positive_rate = 1
            true_positive_rate  = 0


        # aggregate all confusion matricies for final report
        report_info[dirs] = {"cm": cm, 
                             "truth": truth_df, 
                             "pred": pred, 
                             "order": info['odr'],
                             "tpr": true_positive_rate,
                             "fpr": false_positive_rate,
                             "accuracy": acc,
                             "F2": f2,
                             "MCC": mcc}

        # reset class for each loop
        ddd = None
    return report_info

def hit_and_miss_counter(pred, truth):
    # find truth falling and rising edges, should always be equal
    truth_redge = np.flatnonzero((truth[:-1] < 0.5) & (truth[1:] > 0.5))+1
    truth_fedge = np.flatnonzero((truth[:-1] > 0.5) & (truth[1:] < 0.5))+1

    if(truth_redge.shape == truth_fedge.shape):
        truth_hits = np.vstack((truth_redge, truth_fedge))
    else:
        return np.nan, np.nan

    # find predicted falling and rising edges, falling edge might be one short
    pred_redge = np.flatnonzero((pred[:-1] < 0.5) & (pred[1:] > 0.5))+1
    pred_fedge = np.flatnonzero((pred[:-1] > 0.5) & (pred[1:] < 0.5))+1
    
    if(pred_redge.shape == pred_fedge.shape):
        pred_hits = np.vstack((pred_redge, pred_fedge))

    elif((pred_fedge.shape[0] + 1) == pred_redge.shape[0]):
        pred_fedge = np.append(pred_fedge, pred.shape[0])
        pred_hits  = np.vstack((pred_redge, pred_fedge))

    else:
        return np.nan, np.nan

    # loop through truth hits and count hits and remove from pred hits array
    hit_count  = 0
    miss_count = 0
    for t_col in truth_hits.T:
        detected = False
        hit_idx  = []

        for i, p_col in enumerate(pred_hits.T):
            # does the positive predition occur when truth is positive
            if((t_col[0] <= p_col[0] <= t_col[1])
                    or (t_col[0] <= p_col[1] <= t_col[1])):

                detected = True
                hit_idx.append(i)

        if(detected):
            hit_count += 1
            pred_hits = np.delete(pred_hits, hit_idx, 1)

    miss_count = pred_hits.shape[1]

    return hit_count, miss_count

def generate_final_report(prefix, report_info):

        report_info = dict(sorted(report_info.items(), 
                                key=lambda x:operator.getitem(x[1], 'order')))
        ###
        # FIGURE 0 - one large figure
        ###
        run_count         = len(report_info.keys())
        figure_size_tuple = (run_count+4, 4)
        total_hit  = 0   
        total_miss = []
      
        # itterate through runs
        fig = plt.figure(num=0, figsize=(12, 2*run_count))
        for i, (key, value) in enumerate(report_info.items()):
            truth = value['truth']['truth'].to_numpy()
            pred  = value['pred']
            cm    = value['cm']
            row   = value['order']
            hit_cnt, miss_cnt = hit_and_miss_counter(pred, truth)

            total_hit  += np.nan_to_num(hit_cnt)
            total_miss.append(miss_cnt)
            
            # truth vs pred
            axs = plt.subplot2grid(figure_size_tuple, (row, 0), colspan=3)
            if(pred is not None): axs.plot(pred, color='red', label='Prediction')
            axs.plot(truth, color='blue', label='Truth')
            axs.set_title('{}: {}: hits: {}, miss: {}'.format(row, key, hit_cnt, miss_cnt))
            axs.set_xlabel('Frames')
            axs.set_ylim([-0.05, 1.05])
            axs.set_yticks(np.arange(2))
            axs.set_yticklabels(['normal', 'distracted'])
            axs.legend(loc='upper right')

            # cm and metrics 
            axs = plt.subplot2grid(figure_size_tuple, (row, 3), colspan=1)
            if(cm is not None): 
                im  = axs.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
                axs.figure.colorbar(im, ax=axs)
                axs.set_title('Accuracy: {:2.2f}%'.format(100 * value['accuracy']))

                # Rotate the tick labels and set their alignment.
                plt.setp(axs.get_xticklabels(), rotation=45, ha="right",
                         rotation_mode="anchor")

                # Loop over data dimensions and create text annotations.
                fmt = '.2f'
                thresh = cm.max() / 2.
                for i in range(cm.shape[0]):
                    for j in range(cm.shape[1]):
                        axs.text(j, i, format(cm[i, j], fmt),
                                ha="center", va="center",
                                color="white" if cm[i, j] > thresh else "black")

        # ROC plot with a point per run
        axs = plt.subplot2grid(figure_size_tuple, (run_count, 0), colspan=4, rowspan=4)
        axs.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
        # add a point per run
        for key, value in report_info.items():
            if(pred is not None):
                axs.plot(value['fpr'], value['tpr'], 
                            marker='o', markersize=11)
                text_offset = 0.01
                axs.annotate(str(value['order']), 
                            (value['fpr']+ text_offset, 
                            value['tpr']+ text_offset))

        # format
        total_acc = np.mean(np.nan_to_num(np.asarray(
                                [v['accuracy'] for k,v in report_info.items()]
                            )))
        total_mcc = np.mean(np.nan_to_num(np.asarray(
                                [v['MCC'] for k,v in report_info.items()]
                            )))
        total_f2  = np.mean(np.nan_to_num(np.asarray(
                                [v['F2'] for k,v in report_info.items()]
                            )))

        start_y  = 1.01
        dec_y    = 0.04
        start_x  = 0.50
        nstart_x = 0.725
        axs.text(start_x, start_y,
                    'Accuracy:', 
                    color='black')
        axs.text(nstart_x, start_y,
                    '{:2.2f}%'.format(100 * total_acc), 
                    color='black')

        axs.text(start_x, start_y - (1*dec_y),
                    'F2:', 
                    color='black')
        axs.text(nstart_x, start_y - (1*dec_y),
                    '{:2.2f}%'.format(100 * total_f2), 
                    color='black')

        axs.text(start_x, start_y - (2*dec_y), 
                    'MCC:', 
                    color='black')
        axs.text(nstart_x, start_y - (2*dec_y), 
                    '{:2.2f}%'.format(100 * total_mcc), 
                    color='black')

        axs.text(start_x, start_y - (3*dec_y),
                    'Hit Rate:', 
                    color='black')
        axs.text(nstart_x, start_y - (3*dec_y),
                    '{:2.2f}%'.format(100 * (total_hit / run_count)), 
                    color='black')

        axs.text(start_x, start_y - (4*dec_y),
                    'Miss Average:', 
                    color='black')
        axs.text(nstart_x, start_y - (4*dec_y),
                    '{:2.2f}'.format(np.mean(np.nan_to_num(total_miss))), 
                    color='black')

        axs.set_aspect('equal')
        axs.set_title('Receiver Operating Characteristic Space')
        axs.set_xlabel('False Positive Rate')
        axs.set_ylabel('True Positive Rate')

        # save
        fig.tight_layout()
        plt.savefig('{0}/final_report.png'.format(prefix), dpi=400)
        plt.close()

def parse_arguments():
    # parse command line arguments
    parser = argparse.ArgumentParser()

    # arguments 
    parser.add_argument('-i',  '--image', 
                            help='Run on a single image (provide path)') 

    parser.add_argument('-v', '--video',
                            help='Run on a single video (provide path)') 

    parser.add_argument('-p', '--parameters',
                            default='./parameters.yaml',
                            help='YAML file used to store parameters (provide path)') 

    parser.add_argument('-d',  '--debug',  action='store_true', 
                            help='Print extra debug information') 

    parser.add_argument('-t',  '--tune',  action='store_true', 
                            help='Pause playback for algorithm plotting') 

    parser.add_argument('-pp', '--pickle_playback',
                            help='Pickle file used to replay previous run (provide path)') 

    parser.add_argument('-pt', '--pickle_truth',
                            help='Pickle file used for truth comparison (provide path)') 


    parser.add_argument('-sp',  '--skip_plots',  action='store_true', 
                            help='Skip individual run plot generation') 

    parser.add_argument('-bt',  '--batch_test', 
                            help='Loop over a folder consisting of videos and run each\
                                    one (provide path)') 

    parser.add_argument('-abt',  '--auto_batch_test', 
                            help='Loop over pickle files found for previously\
                                    ran videos and search for existing truth\
                                    information for comparison (provide\
                                    path to completed run folders and path\
                                    to truth files with -tf)') 

    parser.add_argument('-tf', '--truth_file',
                            default='../data/truth/labels.json',
                            help='JSON file containing truth label information(provide path)') 

    return parser.parse_args()
        
def main():     
    args = parse_arguments()

    # welcome text
    cprint(figlet_format('DDD !!!', font='starwars'),
            'yellow', 'on_grey', attrs=['bold'])

    # load parameters
    with open(args.parameters) as f:
        parameters = yaml.load(f, Loader=yaml.FullLoader)

    if(args.batch_test):
        # run for all files in provided folder 
        for filename in next(os.walk(args.batch_test))[2]:

            # find images
            if(filename.endswith('.png') and not ('.mp4' in filename)):
                print(filename)

                ddd = DDD(parameters)
                ddd.run(os.path.join(args.batch_test, filename), None, args.debug, args.tune)

            # find videos
            if((filename.endswith('.mp4') or filename.endswith('.m4v')) and 
                    not ("result" in filename)):
                print('Video File: {}'.format(filename))

                ddd = DDD(parameters)
                ddd.run(None, os.path.join(args.batch_test, filename), args.debug, args.tune)
    
            # reset class for each loop
            ddd = None

    elif(args.auto_batch_test):
        if(args.truth_file is None):
            print('NOTE: please specify truth filepath with -tf')
            return
        
        # rerun text
        cprint(figlet_format('Re-Run BATCH!', font='cybermedium'),
                'blue', 'on_grey', attrs=['bold'])

        # create a new directory to host all re-runs
        og_prefix = './batch_rerun'
        prefix    = og_prefix
        i         = 0
        while(os.path.exists(prefix)):
            prefix = og_prefix + '_{0}'.format(i)
            i      = i + 1
        os.mkdir(prefix)

        # run auto batch
        report_info = auto_batch_test(parameters, prefix, 
                                        args.auto_batch_test, args.truth_file, 
                                        args.skip_plots, True)

        # save report
        generate_final_report(prefix, report_info)


    elif(args.pickle_playback):
        # rerun text
        cprint(figlet_format('Re-Run!', font='cybermedium'),
                'blue', 'on_grey', attrs=['bold'])

        # create a new folder for the re-run
        og_prefix = './rerun_ddd'
        prefix    = og_prefix
        i         = 0
        while(os.path.exists(prefix)):
            prefix = og_prefix + '_{0}'.format(i)
            i      = i + 1
        os.mkdir(prefix)

        # load truth data frame
        truth_df = None
        if(args.pickle_truth):
            truth_df = pd.read_pickle(args.pickle_truth)

        # init and run
        ddd   = DDD(parameters, is_live=False, replay=True)
        cm, _ = ddd.debug_run(prefix, args.pickle_playback, truth_df)
        print(cm)

    else:
        # init and run
        ddd = DDD(parameters)
        ddd.run(args.image, args.video, args.debug, args.tune)



if __name__ == '__main__':     
	main()
