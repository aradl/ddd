'''
    Behavior estimation from 
    signal processing and machine learning
'''
# default
import copy
import random

# from pip
import numpy as np
import pandas as pd

# only supports saving to a file
# otherwise we get GTK conflicts with openCV
import matplotlib
matplotlib.use('AGG')
import matplotlib.pyplot as plt

# from this
from ObjectFeatures import ObjFeatures, HistArray

class BehaviorEst():
    """ This class makes an inference about the drivers behavior based on the other processing blocks
    """

    def __init__(self, params, track=0, debug=False):
        self.state = {'normal'     : 0,
                      'abnormal'   : 1,
                      'distracted' : 2
                      }
        self.current_state  = self.state['normal']
        self.next_state     = self.state['normal']
        self.trk            = track
        self.debug          = debug

        # wait time before state machine starts
        self.min_hist_frame  = 30 
        self.state_counter   = 0

        # state machine settings
        self.state_trans_frame = np.zeros(shape=(len(self.state.keys()), len(self.state.keys())),
                                            dtype=np.dtype(int))
        #                       current state,           next state                = min frames for a transition
        self.state_trans_frame[self.state['normal'],     self.state['abnormal']]   = 10
        self.state_trans_frame[self.state['abnormal'],   self.state['normal']]     = 20
        self.state_trans_frame[self.state['abnormal'],   self.state['distracted']] = 10
        self.state_trans_frame[self.state['distracted'], self.state['normal']]     = 20

        # anomaly settings
        anom_conditionals = 2
        self.anom_win     = np.zeros(shape=(len(self.state.keys()), anom_conditionals),
                                            dtype=np.dtype(int))

        # 0 - up, 1 - down
        spar = params['anomaly']['window']
        self.anom_win[self.state['normal'],     0] = spar['normal_up'] 
        self.anom_win[self.state['abnormal'],   0] = spar['abnormal_up'] 
        self.anom_win[self.state['abnormal'],   1] = spar['abnormal_down'] 
        self.anom_win[self.state['distracted'], 1] = spar['distracted_down'] 

        self.anom_score_thresh  = params['anomaly']['score_threshold']

        # cluster settings
        lrn_conditionals = 2
        self.lrn_win     = np.zeros(shape=(len(self.state.keys()), lrn_conditionals),
                                            dtype=np.dtype(int))

        spar = params['learn']['window']
        self.lrn_win[self.state['normal'],     0] = spar['normal_up'] 
        self.lrn_win[self.state['abnormal'],   0] = spar['abnormal_up'] 
        self.lrn_win[self.state['abnormal'],   1] = spar['abnormal_down'] 
        self.lrn_win[self.state['distracted'], 0] = spar['distracted_stay'] 
        self.lrn_win[self.state['distracted'], 1] = spar['distracted_down'] 

        self.lrn_score_min = self.anom_score_thresh * params['learn']['score_threshold_ratio']

        # conditional probabilities
        self.anom_cond = np.zeros(shape=self.anom_win.shape)
        self.lrn_cond  = np.zeros(shape=self.lrn_win.shape)

        spar = params['learn']['probability']
        self.lrn_cond[self.state['normal'], 0]     = spar['normal_up'] 
        self.lrn_cond[self.state['distracted'], 1] = spar['distracted_stay'] 
        self.lrn_cond[self.state['distracted'], 0] = spar['distracted_down'] 

        # history
        self.hist_infer = HistArray(rows=1, dtype=np.uint16)


    def update(self, clu_label, clu_score, clu_size, anom_score, anom_uid, fcnt):
        """ Evaluates, per frame, the anomolous behavior of the driver

            Arguments
                clu_label  [ndarray (1, frames)]           : label of the current frame
                clu_score  [ndarray (cluster_amt, frames)] : score of every cluster
                clu_size   [ndarray (cluster_amt, frames)] : size of every cluster
                anom_score [ndarray (1, frames)]           : score from anomaly detector
                anom_uid   [ndarray (1, frames)]           : frames current UID

            Returns
               uid   [int]: UID of object under evaluation, None if nothing to infer
               infer [int]: objects behavior inference
        """
        # must have a min history amount and valid arrays
        if((fcnt < self.min_hist_frame)
                or (clu_score is None)
                or (clu_label is None)
                or (anom_score is None)):
            return None, None


        # do we have a label for this frame
        if(not np.isnan(clu_label[0, fcnt])):
            # get current label as index
            current_label = int(clu_label[0, fcnt])

            # order clusters by score
            label_byscore_asc = clu_score[:,fcnt].argsort()
        else:
            current_label = None

        # roll the dice
        roll = random.uniform(0, 1)

        # helpers
        score_up = anom_score[(fcnt - self.anom_win[self.current_state, 0]): fcnt]
        score_dn = anom_score[(fcnt - self.anom_win[self.current_state, 1]): fcnt]
        label_up  = clu_label[0, (fcnt - self.lrn_win[self.current_state, 0]):fcnt]
        label_dn  = clu_label[0, (fcnt - self.lrn_win[self.current_state, 1]):fcnt]
       
        current_clu_score = None
        if(current_label is not None):
            current_clu_score = clu_score[current_label, fcnt]

        # state machine
        if(self.current_state == self.state['normal']):
            '''
             Normal
            '''
            uid   = None
            infer = self.state['normal']

            # anom condition - up - windowed score above threshold
            if(np.all(score_up > self.anom_score_thresh)):
                if(self.debug): print('{0}: Norm->Abn, @anom'.format(fcnt))
                self.next_state = self.state['abnormal']

            # are we in a cluster
            if(current_label is not None):
                # clu condition - up - windowed label the same and score above threshold
                if(np.all(label_up == current_label) 
                        and (self.state_counter > self.lrn_win[self.current_state, 0])
                        and (current_clu_score > self.lrn_score_min)
                        and (roll <= self.lrn_cond[self.current_state, 0])):

                    if(self.debug): print('{0}: Norm->Abn, @clu'.format(fcnt))
                    self.next_state = self.state['abnormal']

        elif(self.current_state == self.state['abnormal']):
            '''
             Abnormal
            '''
            uid   = anom_uid[fcnt]
            infer = self.state['abnormal']

            # anom condition - down - windowed score below threshold
            if(np.all(score_dn < self.anom_score_thresh)):
                if(self.debug): print('{0}: Abn->Norm, @anom'.format(fcnt))
                self.next_state = self.state['normal']

            # are we in a cluster
            if(current_label is not None):
                # clu condition - up -current score above threshold and highest label
                if(current_clu_score > self.lrn_score_min
                        and np.any(current_label == label_byscore_asc[-2:])):

                    if(self.debug): print('{0}: Abn->Dis, @clu'.format(fcnt))
                    self.next_state = self.state['distracted']

        elif(self.current_state == self.state['distracted']):
            '''
             Distracted
            '''
            uid   = anom_uid[fcnt]
            infer = self.state['distracted']

            # anom condition - down - windowed score below threshold
            if(np.all(score_dn < self.anom_score_thresh)
                    and (self.state_counter > self.anom_win[self.current_state, 1])):

                if(self.debug): print('{0}: Dis->Norm, @anom'.format(fcnt))
                self.next_state = self.state['normal']

            # are we in a cluster
            if(current_label is not None):
                # clu condition - down - score below threshold
                if(current_clu_score < self.lrn_score_min):
                    if(self.debug): print('{0}: Dis->Norm, @clu'.format(fcnt))
                    self.next_state = self.state['normal']

                # clu condition - down - label change after win and not in top
                if(np.any(label_dn != current_label)
                        and not np.any(current_label == label_byscore_asc[-3:])
                        and (self.state_counter > self.lrn_win[self.current_state, 1])
                        and (roll <= self.lrn_cond[self.current_state, 1])):

                    if(self.debug): print('{0}: Dis->Norm, @clu'.format(fcnt))
                    self.next_state = self.state['normal']

                # clu condition - stay - label is the same - will override above conditionals
                if(np.all(label_up == current_label)
                        and (roll <= self.lrn_cond[self.current_state, 0])):

                    if(self.debug): print('{0}: Dis->Norm, @clu'.format(fcnt))
                    self.next_state = self.state['normal']

        else:
            uid   = None
            infer = None

        # on state change
        self.state_counter = self.state_counter + 1
        if(self.current_state != self.next_state):
            # prevent state change if below min frame time
            if(self.state_counter >= self.state_trans_frame[self.current_state, self.next_state]):
                self.state_counter = 0
            else:
                self.next_state = self.current_state 

        # create history 
        self.hist_infer.update(0, fcnt, infer)

        # change state
        self.current_state = self.next_state
            
        return uid, infer

    def get_pred(self, frame_end):
        if(self.hist_infer.array is not None):
            infer = self.hist_infer.array[0, :frame_end]

            infer[np.isnan(infer)]                   = 0
            infer[infer == self.state['abnormal']]   = 1
            infer[infer == self.state['distracted']] = 1
        else:
            infer = None

        return infer
        

    def plot(self, prefix, features, frame_end):
        """ Plot important metrics ;) """
        
        # create plots
        y    = features.feat['pos_w'].array[0, :frame_end]
        t    = np.linspace(0, y.shape[0], y.shape[0])

        # pull out simple arrays
        # these arrays may not exist
        if(self.hist_infer.array is not None):
            hi = self.hist_infer.array[0, :frame_end]
        else:
            hi = np.zeros(y.shape[0])

        ###
        # FIGURE 0
        ###
        fig = plt.figure(0)
        axs = plt.subplot2grid((1,1), (0,0), rowspan=1)
        axs.plot(hi, linewidth=5)
        axs.set_xlim([0, frame_end])
        axs.set_ylim([-1, 3])
        axs.set_yticks(np.arange(3))
        axs.set_yticklabels(['normal', 'abnormal', 'distracted'])
        axs.set_ylabel('Inference')
        axs.set_xlabel('Frames')
        axs.grid(True)

        fig.tight_layout()
        plt.savefig('{0}/{1}_behav_infer.png'.format(prefix, self.trk), dpi=300)
        plt.close()

