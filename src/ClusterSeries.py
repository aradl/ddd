#!/usr/bin/env python3

# default
import os
import sys
import time
import argparse
import datetime
import warnings
import csv

# from pip
import numpy as np
import pandas as pd
from sklearn.manifold import TSNE
from sklearn import cluster, datasets, mixture
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler
from itertools import cycle, islice
# only supports saving to a file
# otherwise we get GTK conflicts with openCV
import matplotlib as mpl
#mpl.use('AGG')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.cm as cm

# from git
sys.path.append('../utils/catch22/wrap_Python/catch22')
import catch22_C as c22

# from this
from ObjectFeatures import ObjFeatures, HistArray

class ClusterSeries():

    def __init__(self, params, track, debug=False):
        self.sub_features = self.gen_sub_feature_dict()
        self.window_size  = params['sub_features']['window']
        self.trk          = track
        self.start_score  = 1

        # cluster information
        self.clust_size_max   = params['cluster']['size_max'] 
        self.clust_size_min   = params['cluster']['size_min'] 
        self.clust_amt        = params['cluster']['amount'] 
        self.cluster_reset()

        # history
        self.hist_labels = HistArray(rows=1, dtype=np.uint16)
        self.hist_size   = HistArray(rows=self.clust_amt) 
        self.hist_score  = HistArray(rows=self.clust_amt) 

        # cluster metrics
        self.hist_inertia  = HistArray(rows=1)

        # ploting
        self.clust_cmap = plt.get_cmap('tab10')
        self.clust_cmap.set_under('black')
        self.clust_norm = mpl.colors.Normalize(vmin=0, vmax=10)

    def cluster_reset(self):
        # setup cluster
        self.cluster        = cluster.MiniBatchKMeans(n_clusters=self.clust_amt, 
                                                        random_state=0,
                                                        reassignment_ratio=0.02)
        self.clust_begin    = False
        self.clust_sf_index = []

        # accumulated features
        self.sf     = pd.DataFrame(columns=list(self.sub_features.keys()))
        self.labels = None
        
    def gen_sub_feature_dict(self):
        """
        """
        #TODO think about adding the time series vector itself as a sub feature

        c22_sub_feature_list = [ 'DN_HistogramMode_5',
	                             'DN_HistogramMode_10',
	                             'CO_f1ecac',
	                             'CO_FirstMin_ac',
	                             'CO_HistogramAMI_even_2_5',
	                             'CO_trev_1_num',
	                             'MD_hrv_classic_pnn40',
	                             'SB_BinaryStats_mean_longstretch1',
	                             'SB_TransitionMatrix_3ac_sumdiagcov',
	                             'PD_PeriodicityWang_th0_01',
	                             'CO_Embed2_Dist_tau_d_expfit_meandiff',
	                             'IN_AutoMutualInfoStats_40_gaussian_fmmi',
	                             'FC_LocalSimple_mean1_tauresrat',
	                             'DN_OutlierInclude_p_001_mdrmd',
	                             'DN_OutlierInclude_n_001_mdrmd',
	                             'SP_Summaries_welch_rect_area_5_1',
	                             'SB_BinaryStats_diff_longstretch0',
	                             'SB_MotifThree_quantile_hh',
	                             'SC_FluctAnal_2_rsrangefit_50_1_logi_prop_r1',
	                             'SC_FluctAnal_2_dfa_50_1_2_logi_prop_r1',
	                             'SP_Summaries_welch_rect_centroid',
	                             'FC_LocalSimple_mean3_stderr']	

        # generate dict of function calls
        feat_dict = {}
        for f in c22_sub_feature_list:
            feat_dict['c22_' + f] = getattr(c22, f)

        return feat_dict
        

    def calc_sub_features(self, signal):
        """
            Calculate sub features of a signal
        """
        sf = pd.DataFrame(0, columns=list(self.sub_features.keys()), index=[0])

        # interpolate missing information
        # if all are nans return None
        mask   = np.isfinite(signal)
        if(mask.any()):
            si     = np.arange(len(signal))
            signal = np.interp(si, si[mask], signal[mask])

            # shift mean to 0
            signal = signal - np.mean(signal)

            # calculate 
            for key, value in self.sub_features.items():
                try:
                    sf[key] = value(signal.tolist())
                except:
                    print(signal)
        else:
            sf = None

        return sf

    '''
        Limit the size of points for each prediction, but keep
        cluster structure:
            - remove outliers (high varrience)
            - points from low scoreing clusters
    '''
    def prune_sub_features(self, labels):
        #TODO
        pass

    def update(self, fcnt, features, score):
        #TODO calculate for all features ...or not
        #TODO lock down cluster for prediciton only at some point
        if(fcnt < self.window_size):
            return

        f   = features.feat['pos_w']
        if(f.array is None):
            return

        # get subfeatures from signal window
        s  = f.array[self.trk, (fcnt-self.window_size):fcnt]
        sf = self.calc_sub_features(s)
        # if a window is not valid (nans (no track)) 
        # skip adding it to cluster
        if(sf is None):
            return

        sf      = sf.fillna(0)
        self.sf = self.sf.append(sf, ignore_index=True)
        self.clust_sf_index.append(fcnt)

        # once some anomolous behavior is detected
        # start to cluster
        if(score.array[self.trk, fcnt] > self.start_score and
                len(self.sf.index) > self.clust_size_min):
            self.clust_begin = True

        if(self.clust_begin):
            if(self.labels is None):
                # cluster all sub features
                self.labels = self.cluster.fit_predict(self.sf)
            else:
                self.cluster.partial_fit(sf)
                label = self.cluster.predict(sf)
                self.labels = np.append(self.labels, label)

            # find score from when clustering window starts
            sscore = score.array[self.trk, self.clust_sf_index]

            # loop through clusters and update size and score
            for i in range(self.clust_amt):
                label_score  = np.mean(sscore[self.labels==i])
                label_count  = np.sum(self.labels==i)

                self.hist_score.update(i, fcnt, label_score)
                self.hist_size.update(i, fcnt, label_count)

            # save off information
            self.hist_labels.update(0, fcnt, self.labels[-1])
            self.hist_inertia.update(0, fcnt, self.cluster.inertia_)


    def plot(self, prefix, features, frame_end):
        # check is sf exisits (no object found)
        if(self.sf.size < 2):
            return
        
        # create plots
        y    = features.feat['pos_w'].array[self.trk, :frame_end]
        t    = np.linspace(0, y.shape[0], y.shape[0])
        tsne = TSNE(n_components=2, perplexity=15).fit_transform(self.sf)

        # pull out simple arrays
        # these arrays may not exist
        if(self.hist_labels.array is not None):
            cc = self.hist_labels.array[0, :frame_end]
        else:
            cc = np.zeros(y.shape[0])

        if(self.hist_inertia.array is not None):
            ci = self.hist_inertia.array[0, :frame_end]
        else:
            ci = np.zeros(y.shape[0])

        if(self.hist_inertia.array is not None):
            sl = self.hist_score.array[:, :frame_end]
        else:
            sl = np.zeros(shape=(self.clust_amt, y.shape[0]))
        ###
        # FIGURE 0
        ###
        fig = plt.figure(0)
        axs = plt.subplot2grid((5,1), (0,0), rowspan=2)
        # plot signal by class and mark distraction
        ccna  = cc
        ccna[np.isnan(ccna)] = -1
        color = self.clust_cmap(self.clust_norm(ccna))

        axs.scatter(t, y, c=color, s=10)
        axs.set_xlim([0, frame_end])
        axs.set_ylabel('Position [Pixels]')
        axs.set_xlabel('Frames')
        axs.grid(True)

        # plot 2D cluster space
        axs = plt.subplot2grid((5,1), (2,0), rowspan=3)

        ccw  = cc[self.clust_sf_index]
        ccu  = np.unique(ccw)
        ccu  = ccu[~np.isnan(ccu)]
        for c in ccu:
            idx   = (c == ccw)
            color = [self.clust_cmap(self.clust_norm(c))]*np.sum(idx)
            axs.scatter(tsne[idx, 0], tsne[idx, 1], c=color, 
                            label='{:1.0f}'.format(c), s=10)

        axs.set_aspect('equal')
        axs.set_ylabel('Reduced Y')
        axs.set_xlabel('Reduced X')
        axs.legend(loc='center left', bbox_to_anchor=(-1, 0.5), title='Cluster Label')
        axs.grid(True)

        fig.tight_layout()
        plt.savefig('{0}/{1}_tsne_clust.png'.format(prefix, self.trk), dpi=300)
        plt.close()
  
        ###
        # FIGURE 1
        ###
        fig = plt.figure(1)
        dfaxs = self.sf.plot(subplots=True, layout=(11,2), figsize=(16,22), xlim=([0, frame_end]),
                        title='Sub Features')

        for dfax in dfaxs:
            dfax[0].legend(loc='upper right')
            dfax[1].legend(loc='upper right')

        plt.savefig('{0}/{1}_sub_feat.png'.format(prefix, self.trk), dpi=300)
        plt.close()

        ###
        # FIGURE 2
        ###
        fig = plt.figure(2)
        plt.bar(t, ci)
        plt.xlim([0, frame_end])
        axs.set_ylabel('Inertia')
        axs.set_xlabel('Frames')
        axs.grid(True)

        plt.savefig('{0}/{1}_inertia.png'.format(prefix, self.trk), dpi=300)
        plt.close()

        ###
        # FIGURE 3
        ###
        fig = plt.figure(3)
        for i in range(sl.shape[0]):
            plt.plot(t, sl[i], label='{:1.0f}'.format(i))
        plt.xlim([0, frame_end])
        plt.legend(loc='upper left', title='Cluster Label')
        plt.ylabel('Score')
        plt.xlabel('Frames')
        axs.grid(True)

        plt.savefig('{0}/{1}_cluster_score.png'.format(prefix, self.trk), dpi=300)
        plt.close()


    def debug_run(self, pkl_filepath):
        df   = pd.read_pickle(pkl_filepath)
        ittr = len(df.index)

        # only import one track at a time
        features = ObjFeatures(max_track=1, 
                                is_live=False, 
                                buffer_size=ittr)
        score = HistArray(rows=1, 
                               dtype=np.uint8)

        # act on features as if running live
        for index, row in df.iterrows():
            features.update(row['uid'], self.trk, index, 
                            row['area'], (row['pos_w'], row['pos_h']))
            score.update(self.trk, index, row['score'])

            # run cluter
            self.update(index, features, score)
        
        # save off features with updated new score
        prefix_og = './rerun_lrn'
        prefix    = prefix_og
        i         = 0
        while(os.path.exists(prefix)):
            prefix = prefix_og + '_{0}'.format(i)
            i      = i + 1

        os.mkdir(prefix)
        self.plot(prefix, features, ittr)

    '''
     Modified from:
        https://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_comparison.html
    '''
    def cluster_test(self, X, tsne, signal):
        params = {'quantile': .3,
                  'eps': .3,
                  'damping': .9,
                  'preference': -200,
                  'n_neighbors': 10,
                  'n_clusters': 3,
                  'min_samples': 20,
                  'xi': 0.05,
                  'min_cluster_size': 0.1}

        # estimate bandwidth for mean shift
        bandwidth = cluster.estimate_bandwidth(X, quantile=params['quantile'])

        # connectivity matrix for structured Ward
        connectivity = kneighbors_graph(
            X, n_neighbors=params['n_neighbors'], include_self=False)
        # make connectivity symmetric
        connectivity = 0.5 * (connectivity + connectivity.T)

        # ============
        # Create cluster objects
        # ============
        ms = cluster.MeanShift(bandwidth=bandwidth, bin_seeding=True)
        two_means = cluster.MiniBatchKMeans(n_clusters=params['n_clusters'])
        ward = cluster.AgglomerativeClustering(
            n_clusters=params['n_clusters'], linkage='ward',
            connectivity=connectivity)
        spectral = cluster.SpectralClustering(
            n_clusters=params['n_clusters'], eigen_solver='arpack',
            affinity="nearest_neighbors")
        dbscan = cluster.DBSCAN(eps=params['eps'])
        optics = cluster.OPTICS(min_samples=params['min_samples'],
                                xi=params['xi'],
                                min_cluster_size=params['min_cluster_size'])
        affinity_propagation = cluster.AffinityPropagation(
            damping=params['damping'], preference=params['preference'])
        average_linkage = cluster.AgglomerativeClustering(
            linkage="average", affinity="cityblock",
            n_clusters=params['n_clusters'], connectivity=connectivity)
        birch = cluster.Birch(n_clusters=params['n_clusters'])
        gmm = mixture.GaussianMixture(
            n_components=params['n_clusters'], covariance_type='full')

        clustering_algorithms = (
            ('MiniBatchKMeans', two_means),
            ('AffinityPropagation', affinity_propagation),
            ('MeanShift', ms),
            ('SpectralClustering', spectral),
            ('Ward', ward),
            ('AgglomerativeClustering', average_linkage),
            ('DBSCAN', dbscan),
            ('OPTICS', optics),
            ('Birch', birch),
            ('GaussianMixture', gmm)
        )

        for name, algorithm in clustering_algorithms:
            t0 = time.time()

            # catch warnings related to kneighbors_graph
            with warnings.catch_warnings():
                warnings.filterwarnings(
                    "ignore",
                    message="the number of connected components of the " +
                    "connectivity matrix is [0-9]{1,2}" +
                    " > 1. Completing it to avoid stopping the tree early.",
                    category=UserWarning)
                warnings.filterwarnings(
                    "ignore",
                    message="Graph is not fully connected, spectral embedding" +
                    " may not work as expected.",
                    category=UserWarning)
                algorithm.fit(X)

            t1 = time.time()
            if hasattr(algorithm, 'labels_'):
                y_pred = algorithm.labels_.astype(np.int)
            else:
                y_pred = algorithm.predict(X)

            colors = np.array(list(islice(cycle(['#377eb8', '#ff7f00', '#4daf4a',
                                                 '#f781bf', '#a65628', '#984ea3',
                                                 '#999999', '#e41a1c', '#dede00']),
                                          int(max(y_pred) + 1))))
            # add black color for outliers (if any)
            colors = np.append(colors, ["#000000"])

            
            #Class create plots
            y  = signal
            cc = colors[y_pred]
            t  = np.linspace(0, y.shape[0], y.shape[0])

            fig, axs = plt.subplots(2, 1)
            fill = np.ones(y.shape[0] - cc.shape[0])
            axs[0].scatter(t, y, c=cc)
            axs[0].set_ylabel('Position [Pixels]')
            axs[0].set_xlabel('Frames')
            axs[0].grid(True)

            ccu  = np.unique(cc)
            for i, color in enumerate(ccu):
                idx = (color == cc)
                axs[1].scatter(tsne[idx, 0], tsne[idx, 1], c=color, label='cluster {0}'.format(i))
            axs[1].set_ylabel('Reduced Y')
            axs[1].set_xlabel('Reduced X')
            axs[1].set_aspect('equal')
            axs[1].grid(True)
            axs[1].legend(loc="upper right")

            plt.text(.99, .01, ('%.2fs' % (t1 - t0)).lstrip('0'),
                        transform=plt.gca().transAxes, size=15,
                        horizontalalignment='right')

            fig.tight_layout()
            plt.savefig('./plots/{0}_spec.png'.format(name), dpi=500)




def main():     
    # parse command line arguments
    parser = argparse.ArgumentParser()

    # arguments
    parser.add_argument('-p',  '--rerun', 
                            help='Load feature data frame (provide path)') 

    parser.add_argument('-bp',  '--book', 
                            help='Load feature data frame and run a bunch\
                                    of different algorithms (provide path)') 

    args = parser.parse_args()

    lrn   = ClusterSeries(None, track=0) #TODO parameters

    if(args.rerun is not None):
        lrn.debug_run(args.rerun)

    elif(args.book is not None):
        # loop through data frame as if we were running live
        # when running live we will convert from features to a data frame
        # of size window_size
        test        = pd.read_pickle(args.book)
        test_window = lrn.window_size

        sub_features = pd.DataFrame(columns=list(lrn.sub_features.keys()))
        # for book options lets pretend uid doesn't matter
        for i in range(test.shape[0] - test_window):
            sub_test = test.loc[(i-test_window):i, "pos_w"]
            sf       = lrn.calc_sub_features(sub_test)

            sub_features = sub_features.append(sf, ignore_index=True)

        # fill na and normalize
        sub_features = sub_features.fillna(0)
        sub_features_OG = sub_features
        sub_features = StandardScaler().fit_transform(sub_features)

        # t-SNE
        XE = TSNE(n_components=2, perplexity=15).fit_transform(sub_features)

        # thow the book at it
        os.mkdir('./plots')
        lrn.cluster_test(sub_features, XE, test.loc[test_window:, "pos_w"])


if __name__ == '__main__':     
	main()
