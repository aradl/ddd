"""
"""
# default
import os
import sys

# pip
import yaml
import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

# own
sys.path.append('../src')
from ddd import auto_batch_test

class D3E(BaseEstimator, ClassifierMixin):  
    """ class to cast ddd as a sci-kit learn estimator
    """

    def __init__(self, prefix_in):
        # run dirs
        self.run_folder_path = '../data/video'
        self.truth_file      = '../data/truth/labels.json'
        self.prefix_in       = prefix_in

        # load default params
        param_fp = '../src/parameters.yaml'
        with open(param_fp) as f:
            self.params = yaml.load(f, Loader=yaml.FullLoader)

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            setattr(self, parameter, value)

        # override
        self.params['AnomalyScore']['zscore']['norm']['lag']       = self.zscore_norm_lag
        self.params['AnomalyScore']['zscore']['norm']['threshold'] = self.zscore_norm_threshold
        
        return self

    def fit(self, X, y=None):
        return self

    def predict(self, X, y=None):
        return([])

    def score(self, X, y=None):
        # create a new directory to host all re-runs
        og_prefix = '{}/sub_param_opt'.format(self.prefix_in)
        prefix    = og_prefix
        i         = 0
        while(os.path.exists(prefix)):
            prefix = og_prefix + '_{0}'.format(i)
            i      = i + 1
        os.mkdir(prefix)
        self.prefix = prefix

        # run everything
        report_info = auto_batch_test(self.params, self.prefix, 
                                        self.run_folder_path, 
                                        self.truth_file)
        # calculate metrics 
        total_mcc = np.mean(np.nan_to_num(np.asarray(
                                [v['MCC'] for k,v in report_info.items()]
                            )))

        # print status
        print('Run: {}\nFolder: {}\n Performance: {}\nParameters: {}'.format(i, 
                            self.prefix, total_mcc, self.params))

        return total_mcc 



def main():
    """
                                                np.linspace(5, 30, num=5, 
                                                    dtype=np.dtype(int)).tolist(),
    """
    tuned_params = {"zscore_norm_lag"       : [5, 10, 30, 60, 120],
                    "zscore_norm_threshold" : np.linspace(1.5, 2.5, num=5).tolist()}

    print('\n{}\n'.format(tuned_params))
    
    # create a new directory to host all re-runs
    og_prefix = 'param_opt'
    prefix    = og_prefix
    i         = 0
    while(os.path.exists(prefix)):
        prefix = og_prefix + '_{0}'.format(i)
        i      = i + 1
    os.mkdir(prefix)

    gs = GridSearchCV(D3E(prefix_in=prefix), tuned_params,
                        n_jobs=-1, pre_dispatch=10,
                        cv=2)
    gs.fit(np.arange(200).tolist(), np.append(np.ones(100), np.zeros(100)).tolist())

    # print results and save to file
    with open("{}/log.txt".format(prefix), "a") as f: 
        print('Best Score: {}'.format(gs.best_score_), file=f)
        print('Best Parameters:\n\t {}'.format(gs.best_params_), file=f)
        print('Results:\n\t {}'.format(gs.cv_results_), file=f)

if __name__ == '__main__':     
	main()
