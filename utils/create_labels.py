#!/usr/bin/env python3
'''
    Generate truth labels for videos
'''

# default
import os
import argparse
import json

# from pip
import numpy as np
import pandas as pd

def generate_init_json():
    """ Template
    truth_labels[''] = {'trk': 0,
                                            'dis': [(, )]}
    """
    # NOTE labels are subjective
    truth_labels = {}
    truth_labels['D0_C3_4-21-2019_84to90E_None'] = {'trk': 0,
                                                     'dis': None}

    truth_labels['D1_C2_Wht4DS_youtube'] = {'trk': 1,
                                            'dis': [(199, 260)]}

    truth_labels['D1_C4_BluePrius_youtube'] = {'trk': 0,
                                            'dis': [(100, 230)]}

    truth_labels['D2_C3_10-9-19_lowellST_RedSUV'] = {'trk': 0,
                                            'dis': [(230, 320),
                                                     (750, 860)]}

    truth_labels['D3_C2_08-26-19_2nd_PrvRd_Civic_RVS'] = {'trk': 0,
                                            'dis': [(70, 135),
                                                    (200, 285),
                                                    (320, 383)]}

    truth_labels['D2_C2_5-11-2019_BHS_CCtruck'] = {'trk': 0,
                                                     'dis': [(238, 337)]}

    truth_labels['D2_C2_6-27-2019_WPIpark_BrSW'] = {'trk': 0,
                                                     'dis': [(436, 540)]}

    truth_labels['D3_C2_8-14-2019_395S_WhtSUV'] = {'trk': 1,
                                                     'dis': [(590, 790)]}

    truth_labels['D3_C5_8-31-2019_BHS_Blk4DS'] = {'trk': 0,
                                                     'dis': [(365, 480)]}

    truth_labels['D3_C5_8-31-2019_2nd_BHS_Blk4DS'] = {'trk': 0,
                                                     'dis': [(252, 360)]}

    truth_labels['D4_C5_8-31-2019_BHS_Blk4DS'] = {'trk': 0,
                                                     'dis': [(288, 469)]}

    truth_labels['D5_C1_5-11-2019_BHS_CCtruck'] = {'trk': 0,
                                                     'dis': [(320, 335), 
                                                             (515, 643), 
                                                             (740, 940)]}
    truth_labels['D5_C2_5-11-2019_BMS_Blk4DS'] = {'trk': 0,
                                                     'dis': [(150, 230), 
                                                             (250, 450), 
                                                             (695, 780), 
                                                             (785, 950)]}
    truth_labels['D5_C4_8-31-2019_BHS_Blk4DS'] = {'trk': 0,
                                                     'dis': [(248, 333), 
                                                             (490, 600)]}

    return truth_labels



def main():     
    truth_fp = '../data/truth/'
    video_fp = '../data/video/'
    json_fp  = os.path.join(truth_fp, 'labels.json')

    # save json
    if(False):
        truth_labels = generate_init_json()
        if(os.exists(json_fp)):
            print('File exits, you must manually remove')
            return

        with open(json_fp, 'w') as outfile:
            json.dump(truth_labels, outfile, indent=4)
        return

    # load json
    with open(json_fp) as json_file:
        truth_labels = json.load(json_file)

    # search through video for folders and pkl files
    sub_folder_list = [x for x in os.listdir(video_fp) 
                            if os.path.isdir(os.path.join(video_fp, x))]
    run_folder_list = [x for x in sub_folder_list
                            if((x.startswith('D')) and ('_C' in x))]

    for dirs in run_folder_list:
        run_folder = os.path.join(video_fp, dirs)

        # find track files
        feat_fp = [x for x in os.listdir(run_folder)
                        if(x.startswith('trk') and x.endswith('feat.pkl'))]
        feat_fp.sort()

        # move on if nothing found
        if(len(feat_fp) == 0):
            print('NOTE: no pkl file found for {}'.format(dirs))
            continue
           

        if(dirs in truth_labels):
            info      = truth_labels[dirs]
            pickle_fp = os.path.join(video_fp, dirs, feat_fp[info['trk']])

            # load file and generate truth
            df        = pd.read_pickle(pickle_fp)
            frame_end = len(df.index)
            truth     = np.zeros(frame_end, dtype=np.int)

            # mark truth array
            if(info['dis'] is not None):
                for tup in info['dis']:
                    truth[tup[0]:tup[1]] = 1

            # save
            df           = pd.DataFrame({'truth': truth})
            truth_pkl_fp = '{0}/{1}_truth_trk_{2}.pkl'.format(truth_fp, dirs, info['trk'])

            df.to_pickle(truth_pkl_fp)
            print('Created: {}'.format(truth_pkl_fp))

        else:
            print('NOTE: Please create a truth label for {}'.format(dirs))
            continue


if __name__ == '__main__':     
	main()
