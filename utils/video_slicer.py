from moviepy.editor import VideoFileClip

# in seconds
clip = VideoFileClip("../data/video/my_video-2.mkv", audio=False).\
            subclip(69,83)

clip.write_videofile("D2_C2_5-11-2019_BHS_CCtruck.mp4")
