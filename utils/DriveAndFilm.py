#!/usr/bin/env python

import glob
import os
import sys
import threading
import random
import time
import shutil
import multiprocessing

try:
    sys.path.append(glob.glob('../carla/dist/carla-*%d.%d-%s.egg' % (
        sys.version_info.major,
        sys.version_info.minor,
        'win-amd64' if os.name == 'nt' else 'linux-x86_64'))[0])
except IndexError:
    pass

import carla
import skvideo.io

try:
    import pygame
except ImportError:
    raise RuntimeError('cannot import pygame, make sure pygame package is installed')

try:
    import numpy as np
except ImportError:
    raise RuntimeError('cannot import numpy, make sure numpy package is installed')

try:
    import queue
except ImportError:
    import Queue as queue


class DriveAndFilm():

    def __init__(self):
        pygame.init()

        # keys are known working vehicles and list of known good colors
        self.vehicles = {'vehicle.lincoln.mkz2017': [2, 5],
                         'vehicle.nissan.patrol': [0, 2],
                         #'vehicle.audi.a2': [0],
                         'vehicle.tesla.model3': [0]}

        # known good waypoint coordinates pairs
        # 0 - lead car, 1 - tail car : (x, y)
        self.waypoints = {'Town03_loc-0': [(186, -2), (193, -2)],
                          'Town01_loc-0': [(243, 327), (250, 327)],
                          'Town01_loc-1': [(174, 60), (167, 60)]}

        # maneuver variation bounds
        self.drift_bounds  = {'duration': np.linspace(1.0, 2.5, 5),
                              'strength': np.linspace(0.07, -0.07, 4)}
        self.swerve_bounds = {'duration': np.linspace(0.4, 1, 5),
                              'strength': np.linspace(0.50, -0.50, 4)}
        print(self.drift_bounds)

    def run(self, seed=42, maneuver_type='drift'):
        # set random seed
        random.seed(a=seed, version=2)
        postfix = maneuver_type + '_' + str(seed)

        # create a new folder for images
        og_image_folder = './saved_images'
        image_folder    = og_image_folder 
        i = 0
        while(os.path.exists(image_folder)):
            image_folder = og_image_folder + '_{0}'.format(i)
            i = i + 1

        # connect
        client = carla.Client('localhost', 2000)
        client.set_timeout(5.0)
        
        # connect to world
        position_list = [x for x in self.waypoints.keys()]
                           #if(x.startswith(wmap.name))]
        rkey_pos = random.choice(position_list)
        postfix += '_' + rkey_pos

        if(rkey_pos.startswith('Town01')):
            world = client.load_world('Town01')

        elif(rkey_pos.startswith('Town03')): 
            world = client.load_world('Town03')

        wmap  = world.get_map()
        print('using map: ' + wmap.name)
    
        # setup syncronous mode
        print('enabling synchronous mode.')
        settings = world.get_settings()
        settings.synchronous_mode = True
        world.apply_settings(settings)
        world.tick()
    
        actor_list = []
        try:
            blueprint_library = world.get_blueprint_library()
    
            # select vehicle type
            rkey = random.choice(list(self.vehicles.keys()))
            rcol = random.choice(self.vehicles[rkey])
            print('using vehicle: ' + rkey)

            # setup vehicle type
            bp    = blueprint_library.find(rkey)
            color = bp.get_attribute('color').recommended_values[rcol]
            bp.set_attribute('color', color)

            # select starting positions
            rpos = self.waypoints[rkey_pos]
            lead_waypoint = wmap.get_waypoint(
                                carla.Location(x=rpos[0][0], y=rpos[0][1]))
            tail_waypoint = wmap.get_waypoint(
                                carla.Location(x=rpos[1][0], y=rpos[1][1]))
    
            # spawn the lead car
            lead_vehicle = world.spawn_actor(bp, lead_waypoint.transform)
    
            lead_vehicle.set_simulate_physics(True)
            lead_vehicle.set_autopilot(True)
            actor_list.append(lead_vehicle)
            print('created: {} at {}'.format(lead_vehicle.type_id, 
                                            lead_waypoint.transform.location))
    
            # spawn the camera car
            tail_vehicle = world.spawn_actor(bp, tail_waypoint.transform)
    
            tail_vehicle.set_simulate_physics(True)
            actor_list.append(tail_vehicle)
            print('created: {} at {}'.format(tail_vehicle.type_id, 
                                            tail_waypoint.transform.location))
    
            # setup camera
            camera_bp = blueprint_library.find('sensor.camera.rgb')
            #camera_bp.set_attribute('image_size_x', '800')
            #camera_bp.set_attribute('image_size_y', '600')
    
            ## set camera capture rate to 30 FPS
            #camera_bp.set_attribute('sensor_tick', str(1/30))
    
            # attach camera
            camera_transform = carla.Transform(carla.Location(x=1.5, z=1.5))
            camera           = world.spawn_actor(camera_bp, 
                                camera_transform, attach_to=tail_vehicle)
            actor_list.append(camera)
            print('created: {} at {}'.format(camera.type_id, 
                                camera_transform.location))
            
            # setup camera capture
            camera.listen(lambda image:\
                image.save_to_disk(image_folder +\
                '/%06d.png' % image.frame_number))
   
            """
            # set the spectator to spawned location 
            spectator = world.get_spectator()
            spectator.set_transform(tail_waypoint.transform)
            """
   
            """
            #TODO auto pilot settings
            # https://carla.readthedocs.io/en/latest/measurements/
            measurements, sensor_data = carla_client.read_data()
            control = measurements.player_measurements.autopilot_control
            # modify here control if wanted.
            carla_client.send_control(control)
            tail_vehicle.set_autopilot(True)
            """
    
            # Make sync queue for sensor data
            camera_bp        = blueprint_library.find('sensor.camera.rgb')
            camera_transform = carla.Transform(carla.Location(x=-5.5, z=2.8))
            camera           = world.spawn_actor(camera_bp, 
                                    camera_transform, attach_to=tail_vehicle)
            actor_list.append(camera)
            print('created: {} at {}'.format(camera.type_id, 
                                        camera_transform.location))
    
            image_queue = queue.Queue()
            camera.listen(image_queue.put)
    
            # setup display
            frame   = None
            display = pygame.display.set_mode(
                (800, 600),
                pygame.HWSURFACE | pygame.DOUBLEBUF)
            font = self.get_font()
    
            clock      = pygame.time.Clock()
            start_time = pygame.time.get_ticks()

            # setup maneuver
            if(maneuver_type == 'drift'):
                bounds = self.drift_bounds
            else:
                bounds = self.swerve_bounds

            maneuver_start = 7
            maneuver_time  = random.choice(bounds['duration'])
            film_end       = 16
            et = [x*1000 for x in 
                    [maneuver_start, 
                        (maneuver_start + maneuver_time), 
                        film_end]
                 ]
            lead_steer = random.choice(bounds['strength'])
            print(et)
    
            # loop
            while True:
                if self.should_quit():
                    return
   
                # get and advance time
                clock.tick()
                world.tick()
                ts     = world.wait_for_tick()
                eltime = (pygame.time.get_ticks() - start_time)
    
                # alter behavior
                tail_vehicle.apply_control(
                        carla.VehicleControl(throttle=0.6, steer=0.00))
                
                if(maneuver_type == 'drift'):
                    # drift
                    if(et[0] < eltime < et[1]):
                        # execute maneuver
                        lead_vehicle.set_autopilot(False)
                        lead_vehicle.apply_control(
                                carla.VehicleControl(throttle=0.35, steer=lead_steer))
    
                        tail_vehicle.apply_control(
                                carla.VehicleControl(throttle=0.0, steer=0.00))
    
                    elif(et[1] < eltime < et[2]):
                        # correct
                        lead_vehicle.set_autopilot(True)
    
                    elif(et[2] < eltime):
                        # end run
                        break

                else:
                    # swerve
                    if(et[0] < eltime < et[1]):
                        # execute maneuver
                        lead_vehicle.set_autopilot(False)
                        lead_vehicle.apply_control(
                                carla.VehicleControl(throttle=0.5, steer=lead_steer))
    
                        tail_vehicle.apply_control(
                                carla.VehicleControl(throttle=0.0, steer=0.00))
    
                    elif(et[1] < eltime < et[2]):
                        # correct
                        lead_vehicle.set_autopilot(True)
    
                    elif(et[2] < eltime):
                        # end run
                        break
    
                # frame management
                if frame is not None:
                    if ts.frame_count != frame + 1:
                        logging.warning('frame skip!')
    
                frame = ts.frame_count
    
                while True:
                    image = image_queue.get()
                    if image.frame_number == ts.frame_count:
                        break
                    logging.warning(
                        'wrong image time-stampstamp: frame=%d, image.frame=%d',
                        ts.frame_count,
                        image.frame_number)
    
                self.draw_image(display, image)
    
                fps_surface  = font.render('% 5d FPS' % clock.get_fps(), 
                                            True, (255, 255, 255))
                time_surface = font.render(' % 3.2f s' % (eltime / 1000), 
                                            True, (255, 255, 255))
                display.blit(fps_surface,  (8, 10))
                display.blit(time_surface, (8, 23))
    
                pygame.display.flip()
    
        finally:
            print('\ndisabling synchronous mode.')
            settings = world.get_settings()
            settings.synchronous_mode = False
            world.apply_settings(settings)
    
            # clean up
            print('destroying assests')
            for actor in actor_list:
                actor.destroy()
            print('done.')

        return postfix, image_folder

    def exit(self):
        pygame.quit()

    def draw_image(self, surface, image):
        array = np.frombuffer(image.raw_data, dtype=np.dtype("uint8"))
        array = np.reshape(array, (image.height, image.width, 4))
        array = array[:, :, :3]
        array = array[:, :, ::-1]
        image_surface = pygame.surfarray.make_surface(array.swapaxes(0, 1))
        surface.blit(image_surface, (0, 0))
    
    def get_font(self):
        fonts = [x for x in pygame.font.get_fonts()]
        default_font = 'ubuntumono'
        font = default_font if default_font in fonts else fonts[0]
        font = pygame.font.match_font(font)
        return pygame.font.Font(font, 14)
    
    def should_quit(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return True
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_ESCAPE:
                    return True
        return False

def img_to_video(in_q):
    while True:
        try:
            # attempt to get the next item
            item = in_q.get_nowait()

        except queue.Empty:
            break

        else:
            # process item
            postfix, folder_path = item

            # find an unused filename
            print('converting images to video')
            og_filename = 'carla_' + postfix
            filename    = og_filename 
            i           = 0
            while(os.path.exists(filename + '.mp4')):
                filename = og_filename + '_{0}'.format(i)
                i = i + 1
            filename += '.mp4'

            # create video writer 
            # fps    = 30 doesn't seem to matter
            writer = skvideo.io.FFmpegWriter(filename)
            #                                    outputdict={'-r': str(fps)})

            # loop though images
            for (root, _, filenames) in os.walk(folder_path):
                for name in filenames:
                    fname = os.path.join(root, name)

                    if(fname.endswith('.png')):
                        img = skvideo.io.vread(fname)
                        writer.writeFrame(img)
            writer.close()
            shutil.rmtree(folder_path)
            print('finished: {}'.format(postfix))
            time.sleep(.1)

    return True
    
def main():
    file_q = multiprocessing.Queue()

    # start sim actor
    daf   = DriveAndFilm()
    #maneu = 'swerve'
    maneu = 'drift'
    for i in range(127, 138):
        postfix, image_folder = daf.run(seed=i,
                                        maneuver_type=maneu)
        file_q.put((postfix, image_folder))
        time.sleep(10)
    daf.exit()

    # creating processes
    cpu_count = multiprocessing.cpu_count()
    processes = []

    print('multiprocess count: {}'.format(cpu_count // 2))
    for _ in range(cpu_count // 2):
        p = multiprocessing.Process(target=img_to_video, args=(file_q,))
        processes.append(p)
        p.start()

    # completing process
    for p in processes:
        p.join()
    print('processes completed')
    

if __name__ == '__main__':     
	main()
